package com.metamorphosys.interfaces.tranferdataobjects;

import java.util.List;

public class FileUploadResponseTO {
	private String statusCode;
	private String requestId;
	private int responseCode;
	private String responseDescription;
	private List<FileUploadReplyMessageTO> replyMessageList;
	
	public String getStatusCode() {
		return statusCode;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public int getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(int responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDescription() {
		return responseDescription;
	}
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<FileUploadReplyMessageTO> getReplyMessageList() {
		return replyMessageList;
	}
	public void setReplyMessageList(List<FileUploadReplyMessageTO> replyMessageList) {
		this.replyMessageList = replyMessageList;
	}
}
