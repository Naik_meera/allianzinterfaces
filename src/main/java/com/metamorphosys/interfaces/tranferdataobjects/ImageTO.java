package com.metamorphosys.interfaces.tranferdataobjects;

public class ImageTO {
	
	private long imageId;
	private String imageName;
	
	public long getImageId() {
		return imageId;
	}
	public void setImageId(long imageId) {
		this.imageId = imageId;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
}
