package com.metamorphosys.interfaces.tranferdataobjects;

public class RangeTO {
	
	private String min;
	private String max;
	public String getMin() {
		return min;
	}
	public void setMin(String d) {
		this.min = d;
	}

	public String getMax() {
		return max;
	}
	public void setMax(String d) {
		this.max = d;
	}

}
