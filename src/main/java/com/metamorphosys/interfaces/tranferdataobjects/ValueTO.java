package com.metamorphosys.interfaces.tranferdataobjects;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class ValueTO {

	private Integer prorate;
	
	

	public Integer getProrate() {
		return prorate;
	}

	public void setProrate(Integer prorate) {
		this.prorate = prorate;
	}
	
}
