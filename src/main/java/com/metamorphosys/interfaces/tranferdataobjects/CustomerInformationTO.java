package com.metamorphosys.interfaces.tranferdataobjects;

public class CustomerInformationTO {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
