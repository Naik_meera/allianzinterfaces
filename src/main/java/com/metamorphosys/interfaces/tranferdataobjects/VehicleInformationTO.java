package com.metamorphosys.interfaces.tranferdataobjects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

public class VehicleInformationTO {

	private String key;
	private String text;
	private String brand;
	private String area;
	private short seat;
	private String sumInsured;
	private String vehiclePrice;
	private Integer yearManufactured;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public short getSeat() {
		return seat;
	}
	public void setSeat(short seat) {
		this.seat = seat;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String string) {
		this.sumInsured = string;
	}
	public String getVehiclePrice() {
		return vehiclePrice;
	}
	public void setVehiclePrice(String string) {
		this.vehiclePrice = string;
	}
	public Integer getYearManufactured() {
		return yearManufactured;
	}
	public void setYearManufactured(Integer yearManufactured) {
		this.yearManufactured = yearManufactured;
	}
	
	
}
