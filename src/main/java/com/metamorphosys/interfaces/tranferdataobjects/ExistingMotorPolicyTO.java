package com.metamorphosys.interfaces.tranferdataobjects;

public class ExistingMotorPolicyTO {

	private boolean motorPolicyExists;
	private String basicCoverageType;
	private String otherCompany;
	private CompanyTO company;
	private String expiryDate;
	
	public String getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}
	public boolean isMotorPolicyExists() {
		return motorPolicyExists;
	}
	public void setMotorPolicyExists(boolean motorPolicyExists) {
		this.motorPolicyExists = motorPolicyExists;
	}
	public String getBasicCoverageType() {
		return basicCoverageType;
	}
	public void setBasicCoverageType(String basicCoverageType) {
		this.basicCoverageType = basicCoverageType;
	}
	public String getOtherCompany() {
		return otherCompany;
	}
	public void setOtherCompany(String otherCompany) {
		this.otherCompany = otherCompany;
	}
	public CompanyTO getCompany() {
		return company;
	}
	public void setCompany(CompanyTO company) {
		this.company = company;
	}
	
}
