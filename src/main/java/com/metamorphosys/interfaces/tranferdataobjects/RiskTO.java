package com.metamorphosys.interfaces.tranferdataobjects;

public class RiskTO {
	
	
	//"productRiskInfo": {},
	private String buildCategory;
	private String buildYear;
	private String pml;
	private String firstLoss;
	private String remarks;
	private String riskPlan;
	private String elevation;
	private double latitude;
	private double longitude;
	private String location;
	private RiskAdderssTO riskAddress;
	private InterestInsuredListTO interestInsuredList;
	private SurroundingAreaTO surroundingArea;
	public String getBuildCategory() {
		return buildCategory;
	}
	public void setBuildCategory(String buildCategory) {
		this.buildCategory = buildCategory;
	}
	public String getBuildYear() {
		return buildYear;
	}
	public void setBuildYear(String buildYear) {
		this.buildYear = buildYear;
	}
	public String getPml() {
		return pml;
	}
	public void setPml(String pml) {
		this.pml = pml;
	}
	public String getFirstLoss() {
		return firstLoss;
	}
	public void setFirstLoss(String firstLoss) {
		this.firstLoss = firstLoss;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getRiskPlan() {
		return riskPlan;
	}
	public void setRiskPlan(String riskPlan) {
		this.riskPlan = riskPlan;
	}
	public String getElevation() {
		return elevation;
	}
	public void setElevation(String elevation) {
		this.elevation = elevation;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public RiskAdderssTO getRiskAddress() {
		return riskAddress;
	}
	public void setRiskAddress(RiskAdderssTO riskAddress) {
		this.riskAddress = riskAddress;
	}
	public InterestInsuredListTO getInterestInsuredList() {
		return interestInsuredList;
	}
	public void setInterestInsuredList(InterestInsuredListTO interestInsuredList) {
		this.interestInsuredList = interestInsuredList;
	}
	public SurroundingAreaTO getSurroundingArea() {
		return surroundingArea;
	}
	public void setSurroundingArea(SurroundingAreaTO surroundingArea) {
		this.surroundingArea = surroundingArea;
	}
	
}
