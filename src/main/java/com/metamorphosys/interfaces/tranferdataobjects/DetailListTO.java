package com.metamorphosys.interfaces.tranferdataobjects;

import java.util.List;

public class DetailListTO {

	private String valueType;
	private Integer selectedIndex;
	private boolean isEditable;
	private List<String> valueList;
	private boolean display;
	private RangeTO range;
	private List<CoverageSumInsuredRatesTO> coverageSumInsuredRates;
	
	public boolean isDisplay() {
		return display;
	}
	public void setDisplay(boolean display) {
		this.display = display;
	}
	public String getValueType() {
		return valueType;
	}
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
	public Integer getSelectedIndex() {
		return selectedIndex;
	}
	public void setSelectedIndex(Integer selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	public boolean isEditable() {
		return isEditable;
	}
	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}
	public List<String> getValueList() {
		return valueList;
	}
	public void setValueList(List<String> valueList) {
		this.valueList = valueList;
	}
	public List<CoverageSumInsuredRatesTO> getCoverageSumInsuredRates() {
		return coverageSumInsuredRates;
	}
	public void setCoverageSumInsuredRates(List<CoverageSumInsuredRatesTO> coverageSumInsuredRates) {
		this.coverageSumInsuredRates = coverageSumInsuredRates;
	}
	public RangeTO getRange() {
		return range;
	}
	public void setRange(RangeTO range) {
		this.range = range;
	}
	
	
}
