package com.metamorphosys.interfaces.tranferdataobjects;

public class CoverageSumInsuredRatesTO {

	private String sumInsuredFrom;
	private String sumInsuredTo;
	private String premiumRate;
	public String getSumInsuredFrom() {
		return sumInsuredFrom;
	}
	public void setSumInsuredFrom(String string) {
		this.sumInsuredFrom = string;
	}
	public String getSumInsuredTo() {
		return sumInsuredTo;
	}
	public void setSumInsuredTo(String string) {
		this.sumInsuredTo = string;
	}
	public String getPremiumRate() {
		return premiumRate;
	}
	public void setPremiumRate(String string) {
		this.premiumRate = string;
	}
	
	
}
