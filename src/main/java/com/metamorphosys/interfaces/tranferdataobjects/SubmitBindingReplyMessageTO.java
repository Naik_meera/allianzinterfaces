package com.metamorphosys.interfaces.tranferdataobjects;

public class SubmitBindingReplyMessageTO {
	
	private String bbuResults;

	public String getBbuResults() {
		return bbuResults;
	}

	public void setBbuResults(String bbuResults) {
		this.bbuResults = bbuResults;
	}
}
