package com.metamorphosys.interfaces.tranferdataobjects;

public class AccessoryTO {
	
	private String accessoryImageName;
	private String accessoryImageId;
	private String accessoryCode;
	private String accessoryName;
	private String accessoryDesc;
	private Double accessoryAmount;
	
	public String getAccessoryImageName() {
		return accessoryImageName;
	}
	public void setAccessoryImageName(String accessoryImageName) {
		this.accessoryImageName = accessoryImageName;
	}
	public String getAccessoryImageId() {
		return accessoryImageId;
	}
	public void setAccessoryImageId(String accessoryImageId) {
		this.accessoryImageId = accessoryImageId;
	}
	public String getAccessoryCode() {
		return accessoryCode;
	}
	public void setAccessoryCode(String accessoryCode) {
		this.accessoryCode = accessoryCode;
	}
	public String getAccessoryName() {
		return accessoryName;
	}
	public void setAccessoryName(String accessoryName) {
		this.accessoryName = accessoryName;
	}
	public String getAccessoryDesc() {
		return accessoryDesc;
	}
	public void setAccessoryDesc(String accessoryDesc) {
		this.accessoryDesc = accessoryDesc;
	}
	public Double getAccessoryAmount() {
		return accessoryAmount;
	}
	public void setAccessoryAmount(Double accessoryAmount) {
		this.accessoryAmount = accessoryAmount;
	}
}
