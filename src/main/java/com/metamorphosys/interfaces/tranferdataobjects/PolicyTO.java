package com.metamorphosys.interfaces.tranferdataobjects;

public class PolicyTO {
	
	private String userId;
	private String partnerId;
	private String referenceId;
	private String policyStartDate;
	private String policyEndDate;
	private QuotationTO quotation;
	private String agentUserId;
	private float commissionRate;
	private float discountRate;
	private CustomerTO customer;
	private VehicleTO vehicle;
	
	//ALI new fields
	private ExistingMotorPolicyTO existingMotorPolicy;
	private SelectedProductTO selectedProduct;
	
	public float getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(float commissionRate) {
		this.commissionRate = commissionRate;
	}

	public float getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(float discountRate) {
		this.discountRate = discountRate;
	}

	public String getAgentUserId() {
		return agentUserId;
	}

	public void setAgentUserId(String agentUserId) {
		this.agentUserId = agentUserId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getPolicyStartDate() {
		return policyStartDate;
	}

	public void setPolicyStartDate(String policyStartDate) {
		this.policyStartDate = policyStartDate;
	}

	public String getPolicyEndDate() {
		return policyEndDate;
	}

	public void setPolicyEndDate(String policyEndDate) {
		this.policyEndDate = policyEndDate;
	}

	public CustomerTO getCustomer() {
		return customer;
	}

	public void setCustomer(CustomerTO customer) {
		this.customer = customer;
	}

	public VehicleTO getVehicle() {
		return vehicle;
	}

	public void setVehicle(VehicleTO vehicle) {
		this.vehicle = vehicle;
	}

	public QuotationTO getQuotation() {
		return quotation;
	}

	public void setQuotation(QuotationTO quotationTO) {
		this.quotation = quotationTO;
	}

	public ExistingMotorPolicyTO getExistingMotorPolicy() {
		return existingMotorPolicy;
	}

	public void setExistingMotorPolicy(ExistingMotorPolicyTO existingMotorPolicy) {
		this.existingMotorPolicy = existingMotorPolicy;
	}

	public SelectedProductTO getSelectedProduct() {
		return selectedProduct;
	}

	public void setSelectedProduct(SelectedProductTO selectedProduct) {
		this.selectedProduct = selectedProduct;
	}

}
