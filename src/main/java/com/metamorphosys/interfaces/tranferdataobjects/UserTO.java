package com.metamorphosys.interfaces.tranferdataobjects;

public class UserTO {

	private String username;
	private String password;
	private String clientCode;
	private DeviceTO deviceInfo;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getClientCode() {
		return clientCode;
	}
	public void setClientCode(String clientCode) {
		this.clientCode = clientCode;
	}
	public DeviceTO getDeviceInfo() {
		return deviceInfo;
	}
	public void setDeviceInfo(DeviceTO deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
}
