package com.metamorphosys.interfaces.tranferdataobjects;

import java.util.List;

public class VehicleTO {

	private String engineNo;
	private String chassisNo;
	private String plateNo;
	private String mileage;
	private String vehicleUsage;
	private String vehicleDocumentType;
	private int vehicleDocumentTypeImageId;
	private String startDate;
	private String endDate;
	private String accessoryAmount;
	private String accessoryDesc;
	private String accessory;
	private String vehicleDocumentTypeImageName;
	private String vehiclePurchaseDate;
	private String vehicleLocation;
	private VehiclePictureTO vehiclePicture;
	private List<AccessoryTO> accessoryList;

	public String getVehiclePurchaseDate() {
		return vehiclePurchaseDate;
	}

	public void setVehiclePurchaseDate(String vehiclePurchaseDate) {
		this.vehiclePurchaseDate = vehiclePurchaseDate;
	}

	public String getEngineNo() {
		return engineNo;
	}

	public void setEngineNo(String engineNo) {
		this.engineNo = engineNo;
	}

	public String getChassisNo() {
		return chassisNo;
	}

	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}

	public String getPlateNo() {
		return plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getMileage() {
		return mileage;
	}

	public void setMileage(String string) {
		this.mileage = string;
	}

	public String getVehicleUsage() {
		return vehicleUsage;
	}

	public void setVehicleUsage(String vehicleUsage) {
		this.vehicleUsage = vehicleUsage;
	}

	public String getVehicleDocumentType() {
		return vehicleDocumentType;
	}

	public void setVehicleDocumentType(String vehicleDocumentType) {
		this.vehicleDocumentType = vehicleDocumentType;
	}

	public int getVehicleDocumentTypeImageId() {
		return vehicleDocumentTypeImageId;
	}

	public void setVehicleDocumentTypeImageId(int vehicleDocumentTypeImageId) {
		this.vehicleDocumentTypeImageId = vehicleDocumentTypeImageId;
	}

	public VehiclePictureTO getVehiclePicture() {
		return vehiclePicture;
	}

	public void setVehiclePicture(VehiclePictureTO vehiclePicture) {
		this.vehiclePicture = vehiclePicture;
	}

	public List<AccessoryTO> getAccessoryList() {
		return accessoryList;
	}

	public void setAccessoryList(List<AccessoryTO> accessoryList) {
		this.accessoryList = accessoryList;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String string) {
		this.startDate = string;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String string) {
		this.endDate = string;
	}

	public String getAccessoryAmount() {
		return accessoryAmount;
	}

	public void setAccessoryAmount(String accessoryAmount) {
		this.accessoryAmount = accessoryAmount;
	}

	public String getAccessoryDesc() {
		return accessoryDesc;
	}

	public void setAccessoryDesc(String accessoryDesc) {
		this.accessoryDesc = accessoryDesc;
	}

	public String getAccessory() {
		return accessory;
	}

	public void setAccessory(String accessory) {
		this.accessory = accessory;
	}

	public String getVehicleDocumentTypeImageName() {
		return vehicleDocumentTypeImageName;
	}

	public void setVehicleDocumentTypeImageName(String vehicleDocumentTypeImageName) {
		this.vehicleDocumentTypeImageName = vehicleDocumentTypeImageName;
	}

	public String getVehicleLocation() {
		return vehicleLocation;
	}

	public void setVehicleLocation(String vehicleLocation) {
		this.vehicleLocation = vehicleLocation;
	}
	
}
