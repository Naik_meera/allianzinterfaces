package com.metamorphosys.interfaces.tranferdataobjects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

public class ProductTO {

	private String productId;
	private String productName;
	private String currency;
	private String price;
	private String productType;
	
	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	private List<CoverageTO> coverageList;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String string) {
		this.price = string;
	}

	public List<CoverageTO> getCoverageList() {
		return coverageList;
	}

	public void setCoverageList(List<CoverageTO> coverageList) {
		this.coverageList = coverageList;
	}
	
}
