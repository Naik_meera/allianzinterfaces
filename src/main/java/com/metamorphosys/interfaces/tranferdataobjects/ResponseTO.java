package com.metamorphosys.interfaces.tranferdataobjects;

import java.util.List;

public class ResponseTO {

	private String statusCode;
	private List<ReplyMessageTO> replyMessageList;
	
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	public List<ReplyMessageTO> getReplyMessageList() {
		return replyMessageList;
	}
	public void setReplyMessageList(List<ReplyMessageTO> replyMessageList) {
		this.replyMessageList = replyMessageList;
	}
}
