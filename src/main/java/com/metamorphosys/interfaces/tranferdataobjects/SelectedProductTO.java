package com.metamorphosys.interfaces.tranferdataobjects;

public class SelectedProductTO {

	private long displayIndex;
	private long productId;
	private String priceDetails;
	
	public long getDisplayIndex() {
		return displayIndex;
	}
	public void setDisplayIndex(long displayIndex) {
		this.displayIndex = displayIndex;
	}
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public String getPriceDetails() {
		return priceDetails;
	}
	public void setPriceDetails(String priceDetails) {
		this.priceDetails = priceDetails;
	}
}
