package com.metamorphosys.interfaces.tranferdataobjects;

public class AuthErrorMessageTO {
	
	private String erroString;
	
	private String statusCode;

	public String getErroString() {
		return erroString;
	}

	public void setErroString(String erroString) {
		this.erroString = erroString;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

}
