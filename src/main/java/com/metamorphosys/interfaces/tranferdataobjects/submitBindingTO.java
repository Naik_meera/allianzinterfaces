package com.metamorphosys.interfaces.tranferdataobjects;

public class submitBindingTO {
	
	private String userId;
	private String partnerId;
	private QuotationTO quotation;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public QuotationTO getQuotation() {
		return quotation;
	}
	public void setQuotation(QuotationTO quotation) {
		this.quotation = quotation;
	}
	
	
}
