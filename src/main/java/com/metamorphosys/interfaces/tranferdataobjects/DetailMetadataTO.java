package com.metamorphosys.interfaces.tranferdataobjects;

public class DetailMetadataTO {

	private Integer insuredValue;
	private Integer premiumRate;
	private Integer premiumValue;
	private Integer deductible;
	public Integer getInsuredValue() {
		return insuredValue;
	}
	public void setInsuredValue(Integer insuredValue) {
		this.insuredValue = insuredValue;
	}
	public Integer getPremiumRate() {
		return premiumRate;
	}
	public void setPremiumRate(Integer premiumRate) {
		this.premiumRate = premiumRate;
	}
	public Integer getPremiumValue() {
		return premiumValue;
	}
	public void setPremiumValue(Integer premiumValue) {
		this.premiumValue = premiumValue;
	}
	public Integer getDeductible() {
		return deductible;
	}
	public void setDeductible(Integer deductible) {
		this.deductible = deductible;
	}
	
}
