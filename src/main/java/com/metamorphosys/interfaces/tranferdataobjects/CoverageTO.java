package com.metamorphosys.interfaces.tranferdataobjects;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

public class CoverageTO {

	private String coverageCode;
	private String coverageName;
	private String coverageType;
	private Integer displayOrder;
	private boolean isEditable;
	private Integer selectedIndex;
	private String valueType;
	private String coverageGroup;
	private Double prorate;
	
	public Double getProrate() {
		return prorate;
	}

	public void setProrate(Double prorate) {
		this.prorate = prorate;
	}

	private List<String> valueList;
	private DetailMetadataTO detailListMetadata;
	private List<DetailListTO> detailList;

	public String getCoverageCode() {
		return coverageCode;
	}

	public void setCoverageCode(String coverageCode) {
		this.coverageCode = coverageCode;
	}

	public String getCoverageName() {
		return coverageName;
	}

	public void setCoverageName(String coverageName) {
		this.coverageName = coverageName;
	}

	public String getCoverageType() {
		return coverageType;
	}

	public void setCoverageType(String coverageType) {
		this.coverageType = coverageType;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public Integer getSelectedIndex() {
		return selectedIndex;
	}

	public void setSelectedIndex(Integer selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	public String getValueType() {
		return valueType;
	}

	public void setValueType(String valueType) {
		this.valueType = valueType;
	}

	public List<String> getValueList() {
		return valueList;
	}

	public void setValueList(List<String> valueList) {
		this.valueList = valueList;
	}

	public DetailMetadataTO getDetailListMetadata() {
		return detailListMetadata;
	}

	public void setDetailListMetadata(DetailMetadataTO detailListMetadata) {
		this.detailListMetadata = detailListMetadata;
	}

	public String getCoverageGroup() {
		return coverageGroup;
	}

	public void setCoverageGroup(String coverageGroup) {
		this.coverageGroup = coverageGroup;
	}

	public List<DetailListTO> getDetailList() {
		return detailList;
	}

	public void setDetailList(List<DetailListTO> detailList) {
		this.detailList = detailList;
	}
}
