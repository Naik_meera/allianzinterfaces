package com.metamorphosys.interfaces.tranferdataobjects;

public class RiskInformationTO {

	private long postalCode;
	private String city;
	private String sumInsured;
	private String province;
	private String noOfStorey;
	private String eqZone;
	private String floodZone;
	
	private KeyValueTO occupation;
	private KeyValueTO consClass;
	public long getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(long postalCode) {
		this.postalCode = postalCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getNoOfStorey() {
		return noOfStorey;
	}
	public void setNoOfStorey(String noOfStorey) {
		this.noOfStorey = noOfStorey;
	}
	public String getEqZone() {
		return eqZone;
	}
	public void setEqZone(String eqZone) {
		this.eqZone = eqZone;
	}
	public String getFloodZone() {
		return floodZone;
	}
	public void setFloodZone(String floodZone) {
		this.floodZone = floodZone;
	}
	public KeyValueTO getOccupation() {
		return occupation;
	}
	public void setOccupation(KeyValueTO occupation) {
		this.occupation = occupation;
	}
	public KeyValueTO getConsClass() {
		return consClass;
	}
	public void setConsClass(KeyValueTO consClass) {
		this.consClass = consClass;
	}
	
}
