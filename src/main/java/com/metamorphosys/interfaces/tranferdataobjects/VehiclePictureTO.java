package com.metamorphosys.interfaces.tranferdataobjects;

public class VehiclePictureTO {

	private String backLeft;
	private Integer backLeftImageId;
	private String backRight;
	private Integer backRightImageId;
	private String chassisNo;
	private Integer chassisNoImageId;
	private String frontLeft;
	private Integer frontLeftImageId;
	private String frontRight;
	private Integer frontRightImageId;
	
	public String getBackLeft() {
		return backLeft;
	}
	public void setBackLeft(String backLeft) {
		this.backLeft = backLeft;
	}
	public Integer getBackLeftImageId() {
		return backLeftImageId;
	}
	public void setBackLeftImageId(Integer backLeftImageId) {
		this.backLeftImageId = backLeftImageId;
	}
	public String getBackRight() {
		return backRight;
	}
	public void setBackRight(String backRight) {
		this.backRight = backRight;
	}
	public Integer getBackRightImageId() {
		return backRightImageId;
	}
	public void setBackRightImageId(Integer backRightImageId) {
		this.backRightImageId = backRightImageId;
	}
	public String getChassisNo() {
		return chassisNo;
	}
	public void setChassisNo(String chassisNo) {
		this.chassisNo = chassisNo;
	}
	public Integer getChassisNoImageId() {
		return chassisNoImageId;
	}
	public void setChassisNoImageId(Integer chassisNoImageId) {
		this.chassisNoImageId = chassisNoImageId;
	}
	public String getFrontLeft() {
		return frontLeft;
	}
	public void setFrontLeft(String frontLeft) {
		this.frontLeft = frontLeft;
	}
	public Integer getFrontLeftImageId() {
		return frontLeftImageId;
	}
	public void setFrontLeftImageId(Integer frontLeftImageId) {
		this.frontLeftImageId = frontLeftImageId;
	}
	public String getFrontRight() {
		return frontRight;
	}
	public void setFrontRight(String frontRight) {
		this.frontRight = frontRight;
	}
	public Integer getFrontRightImageId() {
		return frontRightImageId;
	}
	public void setFrontRightImageId(Integer frontRightImageId) {
		this.frontRightImageId = frontRightImageId;
	}
	
}
