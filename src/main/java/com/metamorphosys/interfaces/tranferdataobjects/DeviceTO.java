package com.metamorphosys.interfaces.tranferdataobjects;

public class DeviceTO {

	private String browser;
	private String browserVersion;
	private String device;
	private boolean isDesktop;
	private boolean isTablet;
	private boolean isMobile;
	private String os;
	private String osVersion;
	private String userAgent;
	
	public String getBrowser() {
		return browser;
	}
	public void setBrowser(String browser) {
		this.browser = browser;
	}
	public String getBrowserVersion() {
		return browserVersion;
	}
	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getOs() {
		return os;
	}
	public void setOs(String os) {
		this.os = os;
	}
	public String getOsVersion() {
		return osVersion;
	}
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}
	public String getUserAgent() {
		return userAgent;
	}
	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}
	public boolean isDesktop() {
		return isDesktop;
	}
	public void setDesktop(boolean isDesktop) {
		this.isDesktop = isDesktop;
	}
	public boolean isTablet() {
		return isTablet;
	}
	public void setTablet(boolean isTablet) {
		this.isTablet = isTablet;
	}
	public boolean isMobile() {
		return isMobile;
	}
	public void setMobile(boolean isMobile) {
		this.isMobile = isMobile;
	}
}
