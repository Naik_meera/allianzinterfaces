package com.metamorphosys.interfaces.tranferdataobjects;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

public class CustomerTO {

	private String partnerType;
	private String fullName;
	private String dob;
	private String gender;
	private String instituteName;
	private String contactPersonName;
	private String addressLine1;
	private String addressLine2;
	private String postCode;
	private String mobileNo;
	private String telephoneNo;
	private String emailAddress;
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	private String idNo;
	private String nationalIdTypeImageName;
	private Integer nationalIdTypeImageId;
	private boolean nameConfirmation;
	private OccupationTO occupation;
	private CityTO city;
	private ProvinceTO province;
	private CountryTO country;
	private NationalityTO nationalIdType;
	
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String string) {
		this.dob = string;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getInstituteName() {
		return instituteName;
	}
	public void setInstituteName(String instituteName) {
		this.instituteName = instituteName;
	}
	public String getContactPersonName() {
		return contactPersonName;
	}
	public void setContactPersonName(String contactPersonName) {
		this.contactPersonName = contactPersonName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String string) {
		this.mobileNo = string;
	}
	public String getTelephoneNo() {
		return telephoneNo;
	}
	public void setTelephoneNo(String string) {
		this.telephoneNo = string;
	}
	public String getIdNo() {
		return idNo;
	}
	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}
	public String getNationalIdTypeImageName() {
		return nationalIdTypeImageName;
	}
	public void setNationalIdTypeImageName(String nationalIdTypeImageName) {
		this.nationalIdTypeImageName = nationalIdTypeImageName;
	}
	public Integer getNationalIdTypeImageId() {
		return nationalIdTypeImageId;
	}
	public void setNationalIdTypeImageId(Integer string) {
		this.nationalIdTypeImageId = string;
	}
	public OccupationTO getOccupation() {
		return occupation;
	}
	public void setOccupation(OccupationTO occupation) {
		this.occupation = occupation;
	}
	public CityTO getCity() {
		return city;
	}
	public void setCity(CityTO city) {
		this.city = city;
	}
	public ProvinceTO getProvince() {
		return province;
	}
	public void setProvince(ProvinceTO province) {
		this.province = province;
	}
	public CountryTO getCountry() {
		return country;
	}
	public void setCountry(CountryTO country) {
		this.country = country;
	}
	public NationalityTO getNationalIdType() {
		return nationalIdType;
	}
	public void setNationalIdType(NationalityTO nationalIdType) {
		this.nationalIdType = nationalIdType;
	}
	public boolean isNameConfirmation() {
		return nameConfirmation;
	}
	public void setNameConfirmation(boolean nameConfirmation) {
		this.nameConfirmation = nameConfirmation;
	}
	
}
