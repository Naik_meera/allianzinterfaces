package com.metamorphosys.interfaces.tranferdataobjects;

import java.sql.Timestamp;

public class ReplyMessageTO {

	private String displayName;
	private String partnerId;
	private String partnerType;
	private String username;
	private String email;
	private String mobileNumber;
	private String memberClass;
	private Timestamp expireDate;
	private Timestamp lastAccessDate;
	
	private String[] groupList;
	
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getMemberClass() {
		return memberClass;
	}
	public void setMemberClass(String memberClass) {
		this.memberClass = memberClass;
	}
	public Timestamp getExpireDate() {
		return expireDate;
	}
	public void setExpireDate(Timestamp expireDate) {
		this.expireDate = expireDate;
	}
	public Timestamp getLastAccessDate() {
		return lastAccessDate;
	}
	public void setLastAccessDate(Timestamp lastAccessDate) {
		this.lastAccessDate = lastAccessDate;
	}
	public String[] getGroupList() {
		return groupList;
	}
	public void setGroupList(String[] groupList) {
		this.groupList = groupList;
	}
	
}
