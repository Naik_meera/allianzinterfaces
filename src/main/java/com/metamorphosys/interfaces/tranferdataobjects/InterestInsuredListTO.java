package com.metamorphosys.interfaces.tranferdataobjects;

public class InterestInsuredListTO {
	
	private String interestInsuredAmount;
	private String interestInsuredDesc;
	private String interestInsuredName;
	private KeyValueTO interestInsured;
	private ImageTO interestInsuredImage;
	public String getInterestInsuredAmount() {
		return interestInsuredAmount;
	}
	public void setInterestInsuredAmount(String interestInsuredAmount) {
		this.interestInsuredAmount = interestInsuredAmount;
	}
	public String getInterestInsuredDesc() {
		return interestInsuredDesc;
	}
	public void setInterestInsuredDesc(String interestInsuredDesc) {
		this.interestInsuredDesc = interestInsuredDesc;
	}
	public String getInterestInsuredName() {
		return interestInsuredName;
	}
	public void setInterestInsuredName(String interestInsuredName) {
		this.interestInsuredName = interestInsuredName;
	}
	public KeyValueTO getInterestInsured() {
		return interestInsured;
	}
	public void setInterestInsured(KeyValueTO interestInsured) {
		this.interestInsured = interestInsured;
	}
	public ImageTO getInterestInsuredImage() {
		return interestInsuredImage;
	}
	public void setInterestInsuredImage(ImageTO interestInsuredImage) {
		this.interestInsuredImage = interestInsuredImage;
	}
	
}
