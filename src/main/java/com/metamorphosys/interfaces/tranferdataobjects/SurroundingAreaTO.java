package com.metamorphosys.interfaces.tranferdataobjects;

public class SurroundingAreaTO {
	
	private KeyValueTO front;
	private KeyValueTO right;
	private KeyValueTO left;
	private KeyValueTO back;
	
	private ImageTO frontImage;
	private ImageTO rightImage;
	private ImageTO leftImage;
	private ImageTO backImage;
	
	private String unDesiredStock;
	
	public KeyValueTO getFront() {
		return front;
	}
	public void setFront(KeyValueTO front) {
		this.front = front;
	}
	public KeyValueTO getRight() {
		return right;
	}
	public void setRight(KeyValueTO right) {
		this.right = right;
	}
	public KeyValueTO getLeft() {
		return left;
	}
	public void setLeft(KeyValueTO left) {
		this.left = left;
	}
	public KeyValueTO getBack() {
		return back;
	}
	public void setBack(KeyValueTO back) {
		this.back = back;
	}
	public ImageTO getFrontImage() {
		return frontImage;
	}
	public void setFrontImage(ImageTO frontImage) {
		this.frontImage = frontImage;
	}
	public ImageTO getRightImage() {
		return rightImage;
	}
	public void setRightImage(ImageTO rightImage) {
		this.rightImage = rightImage;
	}
	public ImageTO getLeftImage() {
		return leftImage;
	}
	public void setLeftImage(ImageTO leftImage) {
		this.leftImage = leftImage;
	}
	public ImageTO getBackImage() {
		return backImage;
	}
	public void setBackImage(ImageTO backImage) {
		this.backImage = backImage;
	}
	public String getUnDesiredStock() {
		return unDesiredStock;
	}
	public void setUnDesiredStock(String unDesiredStock) {
		this.unDesiredStock = unDesiredStock;
	}
	
}
