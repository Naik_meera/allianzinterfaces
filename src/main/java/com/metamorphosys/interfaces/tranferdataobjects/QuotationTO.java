package com.metamorphosys.interfaces.tranferdataobjects;

public class QuotationTO {

	private Integer selectedIndex;
	private String basicCoverageType;
	private String referenceId;
	private String policyStartDate;
	private String policyEndDate;
	
	private VehicleInformationTO vehicleInformation;
	private ProductTO product;
	private CustomerInformationTO customerInformation;
	private RiskInformationTO riskInformation;
//	private CustomerTO customer;
//	private VehicleTO vehicle;
	
	public Integer getSelectedIndex() {
		return selectedIndex;
	}

	public CustomerInformationTO getCustomerInformation() {
		return customerInformation;
	}

	public void setCustomerInformation(CustomerInformationTO customerInformation) {
		this.customerInformation = customerInformation;
	}

	public RiskInformationTO getRiskInformation() {
		return riskInformation;
	}

	public void setRiskInformation(RiskInformationTO riskInformation) {
		this.riskInformation = riskInformation;
	}

	public void setSelectedIndex(Integer selectedIndex) {
		this.selectedIndex = selectedIndex;
	}

	public String getBasicCoverageType() {
		return basicCoverageType;
	}

	public void setBasicCoverageType(String basicCoverageType) {
		this.basicCoverageType = basicCoverageType;
	}

	public VehicleInformationTO getVehicleInformation() {
		return vehicleInformation;
	}

	public void setVehicleInformation(VehicleInformationTO vehicleInformation) {
		this.vehicleInformation = vehicleInformation;
	}

	public ProductTO getProduct() {
		return product;
	}

	public void setProduct(ProductTO product) {
		this.product = product;
	}

//	public CustomerTO getCustomer() {
//		return customer;
//	}

//	public void setCustomer(CustomerTO customer) {
//		this.customer = customer;
//	}

//	public VehicleTO getVehicle() {
//		return vehicle;
//	}
//
//	public void setVehicle(VehicleTO vehicle) {
//		this.vehicle = vehicle;
//	}

	public CustomerInformationTO getCustomerInformationTO() {
		return customerInformation;
	}

	public void setCustomerInformationTO(CustomerInformationTO customerInformationTO) {
		this.customerInformation = customerInformationTO;
	}

	public String getReferenceId() {
		return referenceId;
	}

	public void setReferenceId(String referenceId) {
		this.referenceId = referenceId;
	}

	public String getPolicyStartDate() {
		return policyStartDate;
	}

	public void setPolicyStartDate(String string) {
		this.policyStartDate = string;
	}

	public String getPolicyEndDate() {
		return policyEndDate;
	}

	public void setPolicyEndDate(String string) {
		this.policyEndDate = string;
	}
	
}
