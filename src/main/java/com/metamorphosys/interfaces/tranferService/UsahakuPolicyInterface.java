package com.metamorphosys.interfaces.tranferService;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.metamorphosys.insureconnect.dataobjects.master.PASProductMapDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDataDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyActivityDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientContactDetailsDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageInsuredDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyResponseDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PremiumSIMappingTableDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.master.CityRepository;
import com.metamorphosys.insureconnect.jpa.master.CountryRepository;
import com.metamorphosys.insureconnect.jpa.master.LookupDataRepository;
import com.metamorphosys.insureconnect.jpa.master.PASProductMapRepository;
import com.metamorphosys.insureconnect.jpa.master.ProvinceRepository;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntityDocumentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyActivityRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyResponseRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PremiumSIMapRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;
import com.metamorphosys.interfaces.tranferdataobjects.CityTO;
import com.metamorphosys.interfaces.tranferdataobjects.CountryTO;
import com.metamorphosys.interfaces.tranferdataobjects.CoverageSumInsuredRatesTO;
import com.metamorphosys.interfaces.tranferdataobjects.CoverageTO;
import com.metamorphosys.interfaces.tranferdataobjects.CustomerInformationTO;
import com.metamorphosys.interfaces.tranferdataobjects.CustomerTO;
import com.metamorphosys.interfaces.tranferdataobjects.DetailListTO;
import com.metamorphosys.interfaces.tranferdataobjects.DetailMetadataTO;
import com.metamorphosys.interfaces.tranferdataobjects.ImageTO;
import com.metamorphosys.interfaces.tranferdataobjects.InterestInsuredListTO;
import com.metamorphosys.interfaces.tranferdataobjects.KeyValueTO;
import com.metamorphosys.interfaces.tranferdataobjects.NationalityTO;
import com.metamorphosys.interfaces.tranferdataobjects.OccupationTO;
import com.metamorphosys.interfaces.tranferdataobjects.PolicyTO;
import com.metamorphosys.interfaces.tranferdataobjects.ProductTO;
import com.metamorphosys.interfaces.tranferdataobjects.ProvinceTO;
import com.metamorphosys.interfaces.tranferdataobjects.QuotationTO;
import com.metamorphosys.interfaces.tranferdataobjects.RangeTO;
import com.metamorphosys.interfaces.tranferdataobjects.RiskTO;
import com.metamorphosys.interfaces.tranferdataobjects.SubmitBindingResponseTO;
import com.metamorphosys.interfaces.tranferdataobjects.SurroundingAreaTO;
import com.metamorphosys.interfaces.tranferdataobjects.VehiclePictureTO;

@RestController
@CrossOrigin
@RequestMapping("/interface/usahaku/policyInterface")
public class UsahakuPolicyInterface {

	private static final Logger log = LoggerFactory.getLogger(PolicyInterface.class);

	@Autowired
	PASProductMapRepository pasProductMapRepository;

	@Autowired
	CityRepository cityRepo;

	@Autowired
	ProvinceRepository provinceRepo;

	@Autowired
	PremiumSIMapRepository premiumSIMapRepository;

	@Autowired
	CountryRepository countryRepo;

	@Autowired
	LookupDataRepository lookupDataRepository;

	@Autowired
	ApplicationRepository applicationRepository;

	@Autowired
	EntityDocumentRepository entityDocRep;

	@Autowired
	RuleRepository ruleRepo;

	@Autowired
	PolicyActivityRepository policyActivityRepository;

	@Autowired
	PolicyResponseRepository policyResponseRepository;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> execute(@RequestBody String json, HttpServletRequest request) {
		PolicyDO policyDO = (PolicyDO) SerializationUtility.getInstance().fromJson(json, PolicyDO.class);
		// List<EntityDocumentDO> entityDocumentList = io.getEntityDocumentDOs();

		// submitBindingTO submitBindingTO = new submitBindingTO();

		PolicyCoverageDO baseplan = policyDO.getBasePlan();
		HashMap constantsHashMap = getPasMappindData("COMMON", "COMMON");
		HashMap basePlanPAS = getPasMappindData(baseplan.getProductCd(), baseplan.getProductCd());

		RiskTO risk = new RiskTO();
		risk.setBuildCategory("");
		risk.setBuildYear("");
		risk.setPml("");
		risk.setFirstLoss("");
		risk.setRemarks("");
		risk.setRiskPlan("");
		risk.setElevation("");
		risk.setLatitude(-6.267053584398342);
		risk.setLongitude(106.80962920188904);
		risk.setLocation("Cilandak, South Jakarta City, Jakarta 12410, Indonesia");
		
		InterestInsuredListTO interestInsuredListTO=new InterestInsuredListTO();
		interestInsuredListTO.setInterestInsuredAmount("");
		interestInsuredListTO.setInterestInsuredDesc("");
		interestInsuredListTO.setInterestInsuredName("");
		
		KeyValueTO interestInsured=new KeyValueTO();
		interestInsured.setKey("");
		interestInsured.setValue("");
		
		ImageTO interestInsuredImage=new ImageTO();
		interestInsuredImage.setImageId(123);
		interestInsuredImage.setImageName("imagename");
		interestInsuredListTO.setInterestInsuredImage(interestInsuredImage);
		
		risk.setInterestInsuredList(interestInsuredListTO);
		
		// QuotationTO
		PolicyTO policyTO = new PolicyTO();
		policyTO.setCommissionRate(policyDO.getCommissionRate());
		policyTO.setDiscountRate(policyDO.getDiscountRate());
		policyTO.setUserId(policyDO.getPolicyAgentList().get(0).getUserId());
		policyTO.setAgentUserId(policyDO.getPolicyAgentList().get(0).getUserId());
		// policyTO.setUserId("bhavini@metamorphtech.com");
		// policyTO.setPartnerId(policyDO.getAgentId());
		policyTO.setPartnerId(policyDO.getPolicyAgentList().get(0).getAgentId());
		// policyTO.setReferenceId("VI-"+getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTID,
		// baseplan.getProductCd(), basePlanPAS)+"
		// "+convertTODate(policyDO.getPolicyCommencementDt())+"-"+"
		// "+policyDO.getProposalNum());
		policyTO.setReferenceId(policyDO.getProposalNum());
		policyTO.setPolicyStartDate(convertTODate(policyDO.getPolicyCommencementDt()));
		policyTO.setPolicyEndDate(convertTODate(policyDO.getPolicyExpiryDt()));

		QuotationTO quotation = new QuotationTO();
		quotation.setSelectedIndex(0);
		// set base coverage map . ID 346 and 366 in pasProductTable
		quotation.setBasicCoverageType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.BASICCOVERAGETYPE,
				baseplan.getCoverageType(), basePlanPAS));
		quotation.setPolicyStartDate(convertTODate(policyDO.getPolicyCommencementDt()));
		quotation.setPolicyEndDate(convertTODate(policyDO.getPolicyExpiryDt()));
		quotation.setReferenceId(policyDO.getProposalNum());

		ProductTO productTO = new ProductTO();
		productTO.setProductId(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTID,
				baseplan.getProductCd(), basePlanPAS));
		productTO.setProductName(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTNAME,
				baseplan.getProductCd(), basePlanPAS));
		// productTO.setPrice(String.format("%.2f",policyDO.getFinalPremium()));
		productTO.setPrice(policyDO.getFinalPremium().toString());

		productTO.setProductType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTTYPE,
				baseplan.getProductCd(), basePlanPAS));
		productTO
				.setCurrency(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.CURRENCY, null, basePlanPAS));

		List<CoverageTO> coverageList = new ArrayList<CoverageTO>();
		int i = 1;
		for (PolicyCoverageDO coverageDO : policyDO.getPolicyCoverageList()) {
			CoverageTO coverageTO = new CoverageTO();
			if (InsureConnectConstants.PLANTYPE.RIDER.equals(coverageDO.getPlanTypeCd())) {
				coverageTO.setCoverageCode(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGECODE,
						coverageDO.getProductCd(), basePlanPAS));
				if (coverageDO.getAssociationType().equals("MANDATORY")) {
					coverageTO.setCoverageType("Y");
				} else {
					coverageTO.setCoverageType("N");
				}
				coverageTO.setCoverageName(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGENAME,
						coverageDO.getProductCd(), basePlanPAS));
			} else {
				coverageTO.setCoverageCode(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGECODE,
						coverageDO.getCoverageType(), basePlanPAS));
				coverageTO.setCoverageType("B");
				coverageTO.setCoverageName(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGENAME,
						coverageDO.getCoverageType(), basePlanPAS));
			}

			coverageTO.setCoverageGroup(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGEGROUP,
					coverageDO.getProductCd(), basePlanPAS));
			coverageTO.setDisplayOrder(i);
			i++;
			if ((coverageDO.getPlanTypeCd().equals("RIDER") && coverageDO.getAssociationType().equals("OPTIONAL"))) {
				// coverageTO.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE,
				// null, coveragePASMap).equals("TRUE")?true:false);
				coverageTO.setEditable(true);
			} else {
				coverageTO.setEditable(false);
			}
			coverageTO.setSelectedIndex(0); // this might need to be changed afterwards based on the value selection of
											// range or list
			// coverageTO.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE,
			// InsureConnectConstants.VALUETYPE.SELECTION, coveragePASMap));
			coverageTO.setValueType("boolean"); // every coverages own value type will always be boolean as it is
												// slected or not

			List<String> list = new ArrayList<String>();
			// if(coverageDO.isSelectedFlag()){
			// list.add(InsureConnectConstants.BOOLEAN.TRUE);
			// }else{
			// list.add(InsureConnectConstants.BOOLEAN.FALSE);
			// }

			if (null != coverageDO.getIsSelected() && coverageDO.getIsSelected().equals("true")) {
				list.add(InsureConnectConstants.BOOLEAN.TRUE);
			} else {
				list.add(InsureConnectConstants.BOOLEAN.FALSE);
			}
			coverageTO.setValueList(list);
			coverageTO.setProrate((double) 1);

			// DetailMetadataTO
			DetailMetadataTO detailMetadataTO = new DetailMetadataTO();
			detailMetadataTO.setInsuredValue(0);
			detailMetadataTO.setPremiumRate(1);
			detailMetadataTO.setPremiumValue(2);
			detailMetadataTO.setDeductible(3);
			coverageTO.setDetailListMetadata(detailMetadataTO);

			// DetailListTO
			List<DetailListTO> detailListTOs = new ArrayList<DetailListTO>();
			// first detailstTO is equivalent of UI meta data for IDV of the car
			DetailListTO detailListTO = new DetailListTO();
			detailListTO.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE,
					InsureConnectConstants.EDITABLEVALUE.INSUREDVALUE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO.setEditable(false);
			detailListTO.setSelectedIndex(0);
			// detailListTO.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE,
			// InsureConnectConstants.EDITABLEVALUE.INSUREDVALUE, coveragePASMap));
			detailListTO.setValueType(coverageDO.getCoverageUIProperty());
			if (detailListTO.getValueType().equals("range")) {
				detailListTO.setRange(getRangeValue(coverageDO));
			}
			;
			List<String> valList = new ArrayList<String>();
			// coverageDO.getSumInsured().toString();
			String sumInsuredVal = String.format("%.0f", coverageDO.getSumInsured());

			valList.add(sumInsuredVal);
			// detailListTO.setValueList(new
			// ArrayList<String>(Arrays.asList(coverageDO.getSumInsured().toString())));
			detailListTO.setValueList(valList);
			detailListTO.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL,
					InsureConnectConstants.EDITABLEVALUE.INSUREDVALUE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO.setDisplay(true);
			detailListTOs.add(detailListTO);

			// for premium rate section
			DetailListTO detailListTO2 = new DetailListTO();
			detailListTO2.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE,
					InsureConnectConstants.EDITABLEVALUE.PREMIUMRATE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO2.setEditable(false);
			detailListTO2.setSelectedIndex(0);

			detailListTO2.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE,
					InsureConnectConstants.EDITABLEVALUE.PREMIUMRATE, basePlanPAS));
			// detailListTO2.setValueType("percentage");
			// detailListTO2.setValueList(new
			// ArrayList<String>(Arrays.asList(coverageDO.getPremiumRate().toString())));
			// detailListTO2.setValueList(new
			// ArrayList<String>(Arrays.asList(String.valueOf(coverageDO.getPremiumRate()*100))));
			detailListTO2.setValueList(new ArrayList<String>(Arrays.asList(String.valueOf(coverageDO.getPremiumRate() * 100))));
			detailListTO2.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL,
					InsureConnectConstants.EDITABLEVALUE.PREMIUMRATE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO2.setDisplay(false);
			detailListTO2.setCoverageSumInsuredRates(getCoverageSumInsuredRate(coverageDO, baseplan, policyDO));

			// coverageSumInsuredRatesTO.se
			detailListTOs.add(detailListTO2);

			DetailListTO detailListTO3 = new DetailListTO();
			detailListTO3.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE,
					InsureConnectConstants.EDITABLEVALUE.PREMIUMVALUE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO3.setEditable(false);
			detailListTO3.setSelectedIndex(0);
			detailListTO3.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE,
					InsureConnectConstants.EDITABLEVALUE.PREMIUMVALUE, basePlanPAS));
			// detailListTO3.setValueType("currency");
			// --------------Decimal Issue Below-------------------
			// detailListTO3.setValueList(new
			// ArrayList<String>(Arrays.asList(String.format("%.2f",coverageDO.getBaseAnnualPremium()))));
			detailListTO3
					.setValueList(new ArrayList<String>(Arrays.asList(coverageDO.getBaseAnnualPremium().toString())));
			// above line is changed because of decimal point issue in NUP changed by
			// siddhant
			detailListTO3.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL,
					InsureConnectConstants.EDITABLEVALUE.PREMIUMVALUE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO3.setDisplay(false);
			detailListTOs.add(detailListTO3);

			// for deductibles section
			DetailListTO detailListTO4 = new DetailListTO();
			detailListTO4.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE,
					InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO4.setEditable(false);
			detailListTO4.setSelectedIndex(0);
			detailListTO4.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE,
					InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE, basePlanPAS));
			// detailListTO4.setValueType("currency-desc");
			detailListTO4.setValueList(new ArrayList<String>(
					Arrays.asList(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUELIST,
							coverageDO.getProductCd() + "-" + InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE,
							basePlanPAS))));
			detailListTO4.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL,
					InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE, basePlanPAS).equals("TRUE") ? true : false);
			// detailListTO4.setDisplay(true);
			detailListTOs.add(detailListTO4);

			coverageTO.setDetailList(detailListTOs);
			coverageList.add(coverageTO);
		}
		if (coverageList.size() > 0) {
			productTO.setCoverageList(coverageList);
		}
		quotation.setProduct(productTO);
		CustomerTO customerTO = new CustomerTO();
		for (PolicyClientDO policyClientDO : policyDO.getPolicyClientList()) {
			if (InsureConnectConstants.ROLECD.PROPOSER.equals(policyClientDO.getRoleCd())) {
				customerTO.setPartnerType(getValuefromHashMap(InsureConnectConstants.CLIENTTYPE.CLIENTCD,
						policyClientDO.getClientTypeCd(), constantsHashMap));
				customerTO.setFullName(policyClientDO.getFirstName());
				customerTO.setNameConfirmation(true);
				customerTO.setInstituteName(policyClientDO.getCompanyName());
				// customerTO.setContactPersonName(policyClientDO.getContactPersonName());

				customerTO.setAddressLine1(policyClientDO.getPolicyClientAddressList().get(0).getAddressLine1());
				customerTO.setAddressLine2(policyClientDO.getPolicyClientAddressList().get(0).getAddressLine2());

				CityTO cityTO = new CityTO();
				cityTO.setCode(policyClientDO.getPolicyClientAddressList().get(0).getCityCd());
				cityTO.setName(cityRepo
						.findByCityCdAndProvinceCd(policyClientDO.getPolicyClientAddressList().get(0).getCityCd(),
								policyClientDO.getPolicyClientAddressList().get(0).getTalukaCd())
						.getCityName());
				customerTO.setCity(cityTO);

				ProvinceTO provinceTO = new ProvinceTO();
				provinceTO.setCode(policyClientDO.getPolicyClientAddressList().get(0).getTalukaCd());
				provinceTO.setName(
						provinceRepo.findByProvinceCd(policyClientDO.getPolicyClientAddressList().get(0).getTalukaCd())
								.getProvinceName());
				customerTO.setProvince(provinceTO);

				CountryTO countryTO = new CountryTO();
				countryTO.setCode(policyClientDO.getPolicyClientAddressList().get(0).getCountryCd());
				countryTO.setName(
						countryRepo.findByCountryCd(policyClientDO.getPolicyClientAddressList().get(0).getCountryCd())
								.getCountryDesc());
				customerTO.setCountry(countryTO);

				customerTO.setPostCode(policyClientDO.getPolicyClientAddressList().get(0).getPostCd().toString());
				if ("P".equals(getValuefromHashMap(InsureConnectConstants.CLIENTTYPE.CLIENTCD,
						policyClientDO.getClientTypeCd(), constantsHashMap))) {
					customerTO.setDob(convertTODate(policyClientDO.getDateOfBirth()));
					customerTO.setContactPersonName("");
					customerTO.setInstituteName("");
				}
				if ("I".equals(getValuefromHashMap(InsureConnectConstants.CLIENTTYPE.CLIENTCD,
						policyClientDO.getClientTypeCd(), constantsHashMap))) {
					customerTO.setIdNo("");
					customerTO.setContactPersonName(policyClientDO.getContactPersonName());
					customerTO.setFullName("");
				}
				customerTO.setGender(getValuefromHashMap(InsureConnectConstants.GENDERCD.GENDERCD,
						policyClientDO.getGenderCd(), constantsHashMap));
				for (PolicyClientContactDetailsDO policyClientContactDetailsDO : policyClientDO
						.getPolicyClientContactDetailsList()) {
					if (InsureConnectConstants.CONTACTTYPE.MOBILE
							.equals(policyClientContactDetailsDO.getContactTypeCd())) {
						customerTO.setMobileNo(policyClientContactDetailsDO == null ? null
								: policyClientContactDetailsDO.getContactNum());
					}
					if (InsureConnectConstants.CONTACTTYPE.HOME
							.equals(policyClientContactDetailsDO.getContactTypeCd())) {
						customerTO.setTelephoneNo(policyClientContactDetailsDO == null ? null
								: policyClientContactDetailsDO.getContactNum());
					}
					if (InsureConnectConstants.CONTACTTYPE.EMAIL
							.equals(policyClientContactDetailsDO.getContactTypeCd())) {
						customerTO.setEmailAddress(policyClientContactDetailsDO == null ? null
								: policyClientContactDetailsDO.getEmailId());
					}
					if (InsureConnectConstants.CONTACTTYPE.OFFICE
							.equals(policyClientContactDetailsDO.getContactTypeCd())) {
						customerTO.setTelephoneNo(policyClientContactDetailsDO == null ? null
								: policyClientContactDetailsDO.getContactNum());
					}

				}

				customerTO.setIdNo(policyClientDO.getIdentityNum());
				List<EntityDocumentDO> documentDOs = entityDocRep.findByEntityIDAndDocumentTypeCd(
						policyDO.getPolicyClientList().get(0).getOfflineGuid(),
						InsureConnectConstants.DOCUMENTYPECD.IDPROOF);
				// List<EntityDocumentDO> documentDOs = entityDocumentList;
				NationalityTO nationalityTO = new NationalityTO();

				if (policyClientDO != null) {

					if (policyClientDO.getIdentityTypeCD() != null) {
						nationalityTO.setCode(policyClientDO.getIdentityTypeCD());
						nationalityTO.setName(lookupDataRepository
								.findByLookupCdAndLookupKey(InsureConnectConstants.LOOKUPCD.IDENTITYTYPE,
										policyClientDO.getIdentityTypeCD())
								.getLookupValue());
						if (policyClientDO.getIdentityTypeCD().equals("PASPOR")) {
							nationalityTO.setCode("PASSPORT");
							nationalityTO.setName("PASSPORT");
						}
						// above code added as advice from Fabian that we have to fix this temperorily
						customerTO.setNationalIdType(nationalityTO);
					}
				}

				if (documentDOs.size() > 0) {

					if (documentDOs.get(0) != null) {
						if (documentDOs.get(0).getReferenceID() != null) {
							customerTO.setNationalIdTypeImageId(Integer.valueOf(documentDOs.get(0).getReferenceID()));
						}

						if (documentDOs.get(0).getEntityImageName() != null) {
							customerTO.setNationalIdTypeImageName(documentDOs.get(0).getEntityImageName());
						}
					}

				}

				OccupationTO occupationTO = new OccupationTO();
				if (policyClientDO != null) {
					// String firstName = policyClientDO.getFirstName();
					String occupation = policyClientDO.getOccupationCd();
					// code to handle null value by Bhavini 07/04/2017
					if (occupation == null) {
						occupationTO.setCode("");
						occupationTO.setName("");
					} else {
						occupationTO.setCode(occupation);
						occupationTO.setName(lookupDataRepository
								.findByLookupCdAndLookupKey(InsureConnectConstants.LOOKUPCD.OCCUPATION, occupation)
								.getLookupValue());
					}
					customerTO.setOccupation(occupationTO);
				}
				policyTO.setCustomer(customerTO);

				// CustomerInformationTO
				CustomerInformationTO customerInformationTO = new CustomerInformationTO();
				customerInformationTO.setName(policyClientDO.getFirstName());
				quotation.setCustomerInformationTO(customerInformationTO);
				break;
			}
		}
		
		SurroundingAreaTO areaTO=new SurroundingAreaTO();
		KeyValueTO front=new KeyValueTO();
		front.setKey("");
		front.setValue("");
		areaTO.setFront(front);
		
		KeyValueTO right=new KeyValueTO();
		front.setKey("");
		front.setValue("");
		areaTO.setRight(right);
		
		KeyValueTO left=new KeyValueTO();
		front.setKey("");
		front.setValue("");
		areaTO.setLeft(left);
		
		KeyValueTO back=new KeyValueTO();
		back.setKey("");
		back.setValue("");
		areaTO.setBack(back);
		
		List<EntityDocumentDO> documentDOs = entityDocRep.findByEntityIDAndDocumentTypeCd(
				baseplan.getPolicyCoverageInsuredList().get(0).getOfflineGuid(),
				InsureConnectConstants.DOCUMENTYPECD.VEHICLE);
		// List<EntityDocumentDO> documentDOs = entityDocumentList;
		VehiclePictureTO pictureTO = new VehiclePictureTO();
		for (EntityDocumentDO entityDocumentDO : documentDOs) {
			// first check whether entityImage null or not. If not null then only proceed
			if (entityDocumentDO.getEntityImage() != null) {
				Integer imageReferenceId = Integer.valueOf(entityDocumentDO.getReferenceID());
				ImageTO image=new ImageTO();
				image.setImageId(imageReferenceId);
				image.setImageName(entityDocumentDO.getEntityImageName());
				if (InsureConnectConstants.DOCSUBTYPECD.FRONT.equals(entityDocumentDO.getDocumentSubTypeCd())) {
					areaTO.setFrontImage(image);
				}else 
				if (InsureConnectConstants.DOCSUBTYPECD.RIGHT.equals(entityDocumentDO.getDocumentSubTypeCd())) {
					areaTO.setRightImage(image);
				}else
				if (InsureConnectConstants.DOCSUBTYPECD.LEFT.equals(entityDocumentDO.getDocumentSubTypeCd())) {
					areaTO.setLeftImage(image);
				}else
				if (InsureConnectConstants.DOCSUBTYPECD.BACK.equals(entityDocumentDO.getDocumentSubTypeCd())) {
					areaTO.setBackImage(image);
				}
			}
		}
		policyTO.setQuotation(quotation);

		// submitBindingTO.setQuotation(quotation);
		String url = applicationRepository.findByKey(InsureConnectConstants.Path.POLICYBINDINGINTERFACEPATHDPATH)
				.getValue();// "https://sit.utamaportal.azindo.allianz.co.id/sit/binding-api/1.0/submit?language=in_ID&channel=web";

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		httpHeaders.set("Authorization", request.getHeader("Authorization"));
		httpHeaders.set("channel", "mobile");

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		HttpEntity<PolicyTO> entity = new HttpEntity<PolicyTO>(policyTO, httpHeaders);

		PolicyActivityDO policyActivityDO = new PolicyActivityDO();
		policyActivityDO.setActivityKey("submit-binding-string-" + policyDO.getProposalRefNum());
		policyActivityDO.setActivityValue(SerializationUtility.getInstance().toJson(policyTO));
		policyActivityDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
		policyActivityDO.setActivityStatus(InsureConnectConstants.Status.PENDING);
		policyActivityRepository.save(policyActivityDO);
		ResponseEntity<String> jsonObject = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);
		// ResponseEntity<String> jsonObject =
		// restTemplate.exchange(queryParamBuilder.build().encode().toUri(),
		// HttpMethod.POST, entity, String.class);
		log.info("PRINT RESPONSE");
		log.info(jsonObject.getBody());

		PolicyResponseDO policyResponseDO = new PolicyResponseDO();
		policyResponseDO.setActivityKey("submit-binding-response-" + policyDO.getProposalRefNum());
		policyResponseDO.setActivityValue(jsonObject.getBody());
		policyResponseDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
		SubmitBindingResponseTO response = (SubmitBindingResponseTO) SerializationUtility.getInstance()
				.fromJson(jsonObject.getBody(), SubmitBindingResponseTO.class);
		if (response.getResponseCode() == 200 && response.getStatusCode().equals("BS000")) {
			policyResponseDO.setActivityStatus(response.getStatusCode() + "-PASSED");
		} else {
			policyResponseDO.setActivityStatus(response.getStatusCode() + "-FAILED");
		}
		policyResponseRepository.save(policyResponseDO);

		HashMap<String, String> submitBindingResultHashMap = new HashMap<String, String>();
		log.info("" + jsonObject.getStatusCode());
		if (!HttpStatus.OK.equals(jsonObject.getStatusCode())) {
			submitBindingResultHashMap.put("submitBindingStatus", "SERVER_ERROR");
			return new ResponseEntity(submitBindingResultHashMap, jsonObject.getStatusCode());
		} else {
			SubmitBindingResponseTO res = (SubmitBindingResponseTO) SerializationUtility.getInstance()
					.fromJson(jsonObject.getBody(), SubmitBindingResponseTO.class);
			if (res.getResponseCode() == 200 && res.getStatusCode().equals("BS000")) {
				submitBindingResultHashMap.put("submitBindingStatus", "PASSED");
				return new ResponseEntity(submitBindingResultHashMap, HttpStatus.OK);
			} else {
				submitBindingResultHashMap.put("submitBindingStatus", "FAILED");
				return new ResponseEntity(submitBindingResultHashMap, HttpStatus.OK);
			}
			// submitBindingResultHashMap.put("submitBindingResult", jsonObject.getBody());
			// return new ResponseEntity(submitBindingResultHashMap, HttpStatus.OK);
		}
		// return jsonObject.getBody();
	}

	public HashMap getPasMappindData(String productCd, String basePlanCd) {
		List<PASProductMapDO> pasProductMapDOs = pasProductMapRepository.findByProductCdAndBasePlanCd(productCd,
				basePlanCd);
		HashMap pasHashMap = new HashMap();
		for (PASProductMapDO pasProductMapDO : pasProductMapDOs) {
			String key = pasProductMapDO.getFieldKey().trim();
			if (pasProductMapDO.getSystemValue() != null) {
				key = key + pasProductMapDO.getSystemValue();
			}

			pasHashMap.put(key, pasProductMapDO.getExternalValue());
		}
		return pasHashMap;
	}

	public String getValuefromHashMap(String fieldKey, String systemValue, HashMap map) {
		String key = fieldKey.trim();
		if (systemValue != null) {
			key = key + systemValue;
		}

		String data = (String) map.get(key);
		return data;
	}

	public List<CoverageSumInsuredRatesTO> getCoverageSumInsuredRateOld(PolicyCoverageDO coverageDO,
			PolicyCoverageDO basePlan, PolicyDO policyDO) {
		List<CoverageSumInsuredRatesTO> coverageSumInsuredRatesTOs = new ArrayList<CoverageSumInsuredRatesTO>();
		RangeTO rangeTO = new RangeTO();
		if ("FLWTHM".equals(coverageDO.getProductCd()) || "EAVTEM".equals(coverageDO.getProductCd())
				|| "STRTCM".equals(coverageDO.getProductCd()) || "TERRSM".equals(coverageDO.getProductCd())
				|| "PADRVR".equals(coverageDO.getProductCd()) || "PAPSGR".equals(coverageDO.getProductCd())
				|| "TAXALL".equals(coverageDO.getProductCd()) || "EERRAA".equals(coverageDO.getProductCd())
				|| "AMBSRV".equals(coverageDO.getProductCd()) || "THBDRV".equals(coverageDO.getProductCd())
				|| "PEREFF".equals(coverageDO.getProductCd()) || "PA24HR".equals(coverageDO.getProductCd())) {
			CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
			// coverageSumInsuredRatesTO.setSumInsuredFrom((double) 0);
			// coverageSumInsuredRatesTO.setSumInsuredTo((double) 1000000000);
			// coverageSumInsuredRatesTO.setPremiumRate(coverageDO.getPremiumRate());
			// coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
			rangeTO.setMin("0");
			rangeTO.setMax("1000000000");
		} else if ("THPRLB".equals(coverageDO.getProductCd()) || "PRLGLB".equals(coverageDO.getProductCd())
				|| "MEDEXP".equals(coverageDO.getProductCd()) || "CSHSTM".equals(coverageDO.getProductCd())
				|| "MOBIMS".equals(coverageDO.getProductCd()) || "VPRKPT".equals(coverageDO.getProductCd())) {
			List<PremiumSIMappingTableDO> mappingTableDOs = premiumSIMapRepository
					.findByProductCd(coverageDO.getProductCd());
			for (PremiumSIMappingTableDO premiumSIMappingTableDO : mappingTableDOs) {
				CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
				coverageSumInsuredRatesTO.setSumInsuredFrom(String.format("%.0f", premiumSIMappingTableDO.getSIFrom()));
				coverageSumInsuredRatesTO.setSumInsuredTo(String.format("%.0f", premiumSIMappingTableDO.getSITo()));
				coverageSumInsuredRatesTO
						.setPremiumRate(String.format("%.0f", premiumSIMappingTableDO.getPremiumRate()));
				coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
			}
		} else if ("AUTWRK".equals(coverageDO.getProductCd())) {
			CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
			coverageSumInsuredRatesTO.setSumInsuredFrom(String.format("%.0f", 0));
			coverageSumInsuredRatesTO.setSumInsuredTo(String.format("%.0f", 1000000000));
			coverageSumInsuredRatesTO
					.setPremiumRate(String.format("%.0f", coverageDO.getPremiumRate() * basePlan.getPremiumRate()));
			coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
		}
		// COMMENTED RULEREPOSITORY METHOD ISSUE
		else if ("MOBECO".equals(coverageDO.getProductCd()) || "MOBGRD".equals(coverageDO.getProductCd())
				|| "NONPKG".equals(coverageDO.getProductCd())) {
			List<RuleDO> ruleDO1 = ruleRepo.findByRuleNameAndDate("BaseCoverRateTable",
					policyDO.getPolicyCommencementDt());
			// RuleDO ruleDO1 = ruleRepo.findByRuleName("BaseCoverRateTable");
			PolicyCoverageInsuredDO vehicle = coverageDO.getPolicyCoverageInsuredList().get(0);
			for (RuleDataDO do1 : ruleDO1.get(0).getRuleDataList()) {
				// Double entryFrom = Double.parseDouble(do1.getCondition4());
				// Double entryTo = Double.parseDouble(do1.getCondition5());
				// if(entryFrom<=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() &&
				// entryTo>=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() ){
				if (Double.parseDouble(do1.getCondition4()) <= vehicle.getEntryAge()
						&& Double.parseDouble(do1.getCondition5()) >= vehicle.getEntryAge()
						&& do1.getCondition3().equals(vehicle.getZoneCd())
						&& do1.getCondition6().equals(vehicle.getVehicleMakeCd())
						&& do1.getCondition7().equals(vehicle.getVehicleModelCd())
						&& do1.getCondition8().equals(basePlan.getCoverageType())) {
					CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
					coverageSumInsuredRatesTO.setSumInsuredFrom(String.format("%.0f", do1.getCondition1()));
					coverageSumInsuredRatesTO.setSumInsuredTo(String.format("%.0f", do1.getCondition2()));
					coverageSumInsuredRatesTO.setPremiumRate(String.format("%.0f", do1.getAction1()));
					coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
				}
			}
			// do nothing by swapnil 2nd feb 2017
		}

		return coverageSumInsuredRatesTOs;
	}

	public List<CoverageSumInsuredRatesTO> getCoverageSumInsuredRate(PolicyCoverageDO coverageDO,
			PolicyCoverageDO basePlan, PolicyDO policyDO) {
		List<CoverageSumInsuredRatesTO> coverageSumInsuredRatesTOs = new ArrayList<CoverageSumInsuredRatesTO>();
		RangeTO rangeTO = new RangeTO();
		if (coverageDO.getPlanTypeCd().equals(InsureConnectConstants.PLANTYPE.BASEPLAN)) {
			List<RuleDO> ruleDO1 = ruleRepo.findByRuleNameAndDate("BaseCoverRateTable",
					policyDO.getPolicyCommencementDt());
			// RuleDO ruleDO1 = ruleRepo.findByRuleName("BaseCoverRateTable");
			PolicyCoverageInsuredDO vehicle = basePlan.getPolicyCoverageInsuredList().get(0);

			for (RuleDataDO do1 : ruleDO1.get(0).getRuleDataList()) {
				Double entryFrom = Double.parseDouble(do1.getCondition4());
				Double entryTo = Double.parseDouble(do1.getCondition5());
				// if(entryFrom<=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() &&
				// entryTo>=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() ){
				if (Double.parseDouble(do1.getCondition4()) <= vehicle.getEntryAge()
						&& Double.parseDouble(do1.getCondition5()) >= vehicle.getEntryAge()
						&& do1.getCondition3().equals(vehicle.getZoneCd())
						&& do1.getCondition6().equals(vehicle.getVehicleMakeCd())
						&& do1.getCondition7().equals(vehicle.getVehicleModelCd())
						&& do1.getCondition8().equals(basePlan.getCoverageType())) {
					CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
					coverageSumInsuredRatesTO.setSumInsuredFrom(do1.getCondition1());
					coverageSumInsuredRatesTO.setSumInsuredTo(do1.getCondition2());
					coverageSumInsuredRatesTO.setPremiumRate(do1.getAction1());
					coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
				}
			}
			// do nothing by swapnil 2nd feb 2017
		}
		if (coverageDO.getPlanTypeCd().equals(InsureConnectConstants.PLANTYPE.RIDER)) {
			if (coverageDO.getCoverageUIProperty().equals("list")) {
				CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
				coverageSumInsuredRatesTO.setSumInsuredFrom("0");
				coverageSumInsuredRatesTO.setSumInsuredTo("1000000000");
				coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);

			}
			if (coverageDO.getCoverageUIProperty().equals("range")) {
				if (coverageDO.getProductCd().equals("PADRVR") || coverageDO.getProductCd().equals("PAPSGR")) {
					CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
					coverageSumInsuredRatesTO.setSumInsuredFrom("0");
					coverageSumInsuredRatesTO.setSumInsuredTo("1000000000");
					coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
				} else {
					// List<RuleDataDO> sumInsuredList =
					// ruleRepo.findByRuleNameAndDate(ruleRepo.findByRuleNameAndDate(basePlan.getProductCd()+coverageDO.getProductCd()+"PREMIUMRATE",
					// policyDO.getPolicyCommencementDt()).get(0).getRuleDataList().get(0).getAction1(),policyDO.getPolicyCommencementDt()).get(0).getRuleDataList();
					List<RuleDO> firstRule = ruleRepo.findByRuleNameAndDate(
							basePlan.getProductCd() + coverageDO.getProductCd() + "PREMIUMRATE",
							policyDO.getPolicyCommencementDt());
					RuleDataDO dataOfFirstRule = firstRule.get(0).getRuleDataList().get(0);
					String nexdtRuleName = dataOfFirstRule.getAction1();
					List<RuleDO> secondRule = ruleRepo.findByRuleNameAndDate(nexdtRuleName,
							policyDO.getPolicyCommencementDt());
					List<RuleDataDO> dataOfSecondRule = secondRule.get(0).getRuleDataList();
					for (RuleDataDO ruleDataDO : dataOfSecondRule) {
						CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
						coverageSumInsuredRatesTO.setSumInsuredFrom(ruleDataDO.getCondition1());
						coverageSumInsuredRatesTO.setSumInsuredTo(ruleDataDO.getCondition2());
						coverageSumInsuredRatesTO.setPremiumRate(ruleDataDO.getAction1());
						coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
					}
				}
			}
			if (coverageDO.getCoverageUIProperty().equals("currency")) {
				CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
				coverageSumInsuredRatesTO.setSumInsuredFrom("0");
				coverageSumInsuredRatesTO.setSumInsuredTo("1000000000");
				coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
			}
		}

		return coverageSumInsuredRatesTOs;
	}

	public String convertTODate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String format = formatter.format(date);
		return format;
	}

	public RangeTO getRangeValue(PolicyCoverageDO coverageDO) {
		RangeTO range = new RangeTO();

		if (coverageDO.getCoverageUIPropertyValue().contains("-")) {
			range.setMin(coverageDO.getCoverageUIPropertyValue().split("-")[1]);
			range.setMax(coverageDO.getCoverageUIPropertyValue().split("-")[2]);
		} else {
			range.setMin(coverageDO.getCoverageUIPropertyValue());
			range.setMax(coverageDO.getCoverageUIPropertyValue());
		}

		return range;
	}

}
