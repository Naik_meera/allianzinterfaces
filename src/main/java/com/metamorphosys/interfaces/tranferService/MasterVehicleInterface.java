package com.metamorphosys.interfaces.tranferService;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.metamorphosys.insureconnect.dataobjects.master.DataSyncDetailsDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.master.DataSyncDetailsRepository;
import com.metamorphosys.insureconnect.transferobjects.MasterVehicleResponseTO;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;

@RestController
@CrossOrigin
@RequestMapping("/interface/syncVehicles")
public class MasterVehicleInterface {
	
	private static final Logger log = LoggerFactory.getLogger(MasterVehicleInterface.class);
	
	@Autowired
	DataSyncDetailsRepository dataSyncDetailsRepository;
	
	@Autowired
	ApplicationRepository applicationRepository;
	
	@RequestMapping(method = RequestMethod.GET)
	public MasterVehicleResponseTO execute(HttpServletRequest request){
		
		String authToken = "";
		String systemUserId = "";
		String systemUserPwd = "";
		systemUserId = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERID).getValue();
		systemUserPwd = applicationRepository.findByKey(InsureConnectConstants.SystemUserCredentials.SYSTEMUSERPWD).getValue();

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.set("channel", "mobile");
		HttpEntity entityauth = new HttpEntity(httpHeaders);

		String internalAuthURL = applicationRepository.findByKey(InsureConnectConstants.Path.AUTHINTERFACEPATH).getValue()
				+ "/" + systemUserId + "/" + systemUserPwd;
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<String> reponseObject = restTemplate.exchange(internalAuthURL, HttpMethod.GET, entityauth, String.class);
		log.info("Getting auth token from "+internalAuthURL);
		if(HttpStatus.OK.equals(reponseObject.getStatusCode())) {
			HashMap<String, String> excternalAuthAPIresponse = (HashMap<String, String>) SerializationUtility.getInstance()
					.fromJson(reponseObject.getBody(), HashMap.class);
			authToken = excternalAuthAPIresponse.get("authToken");
		}
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Timestamp lastUpdatedDate=null;
		List<DataSyncDetailsDO> dataSyncDate=null;
		try{
			dataSyncDate=dataSyncDetailsRepository.findAll();
		for(DataSyncDetailsDO dataSyncDetailsDO:dataSyncDate){
			 lastUpdatedDate=dataSyncDetailsDO.getLastUpdatedDate();
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		String url =applicationRepository.findByKey(InsureConnectConstants.Path.VEHICLESYNCINTERFACEPATH).getValue();
		restTemplate = new RestTemplate();
		UriComponentsBuilder queryParamBuilder = UriComponentsBuilder.fromHttpUrl(url);
		queryParamBuilder.queryParam("channel", "mobile");
		queryParamBuilder.queryParam("language", "in_ID");
		queryParamBuilder.queryParam("lastDate", dateFormat.format(lastUpdatedDate));
		queryParamBuilder.queryParam("toDate", dateFormat.format(new Date()));
		httpHeaders = new HttpHeaders();
		httpHeaders.set("Authorization", authToken);
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(httpHeaders);
		log.info("Getting data from "+queryParamBuilder.build().encode().toUri());
		ResponseEntity<String> responseObject = restTemplate.exchange(queryParamBuilder.build().encode().toUri(),HttpMethod.GET, entity, String.class);
		//log.info("Response "+responseObject);
		MasterVehicleResponseTO responseTO= (MasterVehicleResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), MasterVehicleResponseTO.class);
		
		return responseTO;
	}
	
}
