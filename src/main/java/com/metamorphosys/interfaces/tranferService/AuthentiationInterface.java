package com.metamorphosys.interfaces.tranferService;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.metamorphosys.insureconnect.dataobjects.transaction.AgentDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.transferobjects.TokenValidationResponseTO;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;
import com.metamorphosys.interfaces.tranferdataobjects.AuthErrorMessageTO;
import com.metamorphosys.interfaces.tranferdataobjects.DeviceTO;
import com.metamorphosys.interfaces.tranferdataobjects.ReplyMessageTO;
import com.metamorphosys.interfaces.tranferdataobjects.ResponseTO;
import com.metamorphosys.interfaces.tranferdataobjects.UserTO;

@RestController
@RequestMapping("/interface")
public class AuthentiationInterface {
	
	private static final Logger log = LoggerFactory.getLogger(AuthentiationInterface.class);
	
	@Autowired
	ApplicationRepository applicationRepository;

	@RequestMapping(method = RequestMethod.GET,value="/authenticate/{userId}/{password}")
	public ResponseEntity<String> execute(@PathVariable String userId,@PathVariable String password){
		
		log.info("Forwarding Auth Request");
		
		UserTO userTO = new UserTO();
		userTO.setUsername(userId);
		userTO.setPassword(password);
		userTO.setClientCode("12023");
		DeviceTO deviceTO = new DeviceTO();
		deviceTO.setBrowser("chrome");
		deviceTO.setBrowserVersion("55.0.2883.87");
		deviceTO.setDevice("unknown");
		deviceTO.setTablet(false);
		deviceTO.setMobile(false);
		deviceTO.setDesktop(true);
		deviceTO.setOs("windows");
		deviceTO.setOsVersion("windows 7 Professional");
		deviceTO.setUserAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87");
		userTO.setDeviceInfo(deviceTO);
		String url =applicationRepository.findByKey(InsureConnectConstants.Interface.AUTHPATH).getValue();
		RestTemplate restTemplate = new RestTemplate();
		UriComponentsBuilder queryParamBuilder = UriComponentsBuilder.fromHttpUrl(url);
		queryParamBuilder.queryParam("channel", "mobile");
		queryParamBuilder.queryParam("language", "in_ID");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.set("channel", "mobile");
		HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(userTO), httpHeaders);
		log.info("Sending request to NUP "+queryParamBuilder.build().encode().toUri());
		ResponseEntity<String> responseObject = restTemplate.exchange(queryParamBuilder.build().encode().toUri(),HttpMethod.POST, entity, String.class);
		log.info("Response from NUP "+responseObject);
		if(HttpStatus.OK.equals(responseObject.getStatusCode())){
			HashMap<String, String> hashMap = new HashMap<String, String>();
			String authToken = responseObject.getHeaders().getFirst("Authorization");
		//	HttpHeaders headers = responseObject.getHeaders();
			hashMap.put("authToken", authToken);
			
			ResponseTO responseTO= (ResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), ResponseTO.class);
			AgentDO agentDO = new AgentDO();
			AuthErrorMessageTO errMessage = new AuthErrorMessageTO();
			hashMap.put("statusCode", responseTO.getStatusCode());
			
			
			if(HttpStatus.OK.equals(responseObject.getStatusCode())){
				if(InsureConnectConstants.InterfaceStatusCd.ERR_INVALIDUSER.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_INVALIDUSER);
				}else if(InsureConnectConstants.InterfaceStatusCd.ERR_ACCOUNTEXPIRE.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_ACCOUNTEXPIRE);
				}else if(InsureConnectConstants.InterfaceStatusCd.ERR_CREDENTIALEXPIRE.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_CREDENTIALEXPIRE);
				}else if(InsureConnectConstants.InterfaceStatusCd.ERR_APPPERMISSION.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_APPPERMISSION);
				}else if(InsureConnectConstants.InterfaceStatusCd.ERR_AUTH.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_AUTH);
				}else if(InsureConnectConstants.InterfaceStatusCd.ERR_CROWDCONN.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_CROWDCONN);
				}else if(InsureConnectConstants.InterfaceStatusCd.ERR_SERVICE.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_SERVICE);
				}else if(InsureConnectConstants.InterfaceStatusCd.ERR_LOCKED.equals(responseTO.getStatusCode())){
					hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_LOCKED);
				}else{
					ReplyMessageTO messageTO= responseTO.getReplyMessageList().get(0);
					if(messageTO != null){
						agentDO.setAgentId(messageTO.getPartnerId());
						agentDO.setLastloginDt(messageTO.getLastAccessDate());
						agentDO.setAgentFirstName(messageTO.getDisplayName());
						if(messageTO.getUsername()!=null)
						{
							agentDO.setUserId(messageTO.getUsername().toLowerCase());
						}
						agentDO.setMobileNumber(messageTO.getMobileNumber());
						agentDO.setEmailId(messageTO.getEmail());
						agentDO.setLicenseExpiryDt(messageTO.getExpireDate());
						agentDO.setAgentCategory(messageTO.getMemberClass().toUpperCase());
						String groupList = "";
						for(int i=0; i< messageTO.getGroupList().length; i++){
							if(i == messageTO.getGroupList().length-1){
								groupList = groupList+messageTO.getGroupList()[i];
							}else{
								groupList = " , "+groupList+messageTO.getGroupList()[i]+" , ";
							}
						}
						agentDO.setGroupList(groupList);
						hashMap.put("AgentDO", SerializationUtility.getInstance().toJson(agentDO));	
					}else{
						hashMap.put("ERRORCD", InsureConnectConstants.EXCEPTION.ERR_INVALIDUSER);
					}
				}
				//hashMap.put("ERROR_CD", SerializationUtility.getInstance().toJson(InsureConnectConstants.EXCEPTION.ERR_UNKNOWN));
				errMessage.setErroString(hashMap.get("ERRORCD"));
				errMessage.setStatusCode(hashMap.get("statusCode"));				
				hashMap.put("ERROR_CD", SerializationUtility.getInstance().toJson(errMessage));
				String jsonResult = SerializationUtility.getInstance().toJson(hashMap);
				log.info("Returning Back Auth Request");
				return new ResponseEntity(jsonResult, HttpStatus.OK);
			}
		}else{
				log.info("Returning Back Auth Request");
				return new ResponseEntity(responseObject.getStatusCode());
		}
		return null;
	}
	
	@RequestMapping(method = RequestMethod.POST,value="/validateToken")
	public TokenValidationResponseTO validateToken(HttpServletRequest request){
		String url =applicationRepository.findByKey(InsureConnectConstants.Path.TOKENVALIDATIONINTERFAEPATH).getValue();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.set("Authorization",request.getHeader("Authorization"));
		HttpEntity<String> entity = new HttpEntity<String>("", httpHeaders);
		log.info("Sending request to NUP "+url);
		try {
			ResponseEntity<String> responseObject = restTemplate.exchange(url,HttpMethod.POST, entity, String.class);
			log.info("Response from NUP "+responseObject);
			TokenValidationResponseTO responseTO= (TokenValidationResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), TokenValidationResponseTO.class);
			return responseTO;
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@RequestMapping(method = RequestMethod.GET,value="/inValidateToken")
	public String inValidateToken(HttpServletRequest request){
		String url =applicationRepository.findByKey(InsureConnectConstants.Path.TOKENINVALIDATIONINTERFAEPATH).getValue();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.set("Authorization",request.getHeader("Authorization"));
		HttpEntity<String> entity = new HttpEntity<String>("", httpHeaders);
		log.info("Sending request to NUP "+url);
		try {
			ResponseEntity<String> responseObject = restTemplate.exchange(url,HttpMethod.GET, entity, String.class);
			log.info("Response from NUP "+responseObject);
			TokenValidationResponseTO responseTO= (TokenValidationResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), TokenValidationResponseTO.class);
		}catch(Exception e) {
		}
		return "";
	}
}
