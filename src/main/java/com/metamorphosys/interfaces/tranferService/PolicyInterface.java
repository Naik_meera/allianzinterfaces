package com.metamorphosys.interfaces.tranferService;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.metamorphosys.insureconnect.dataobjects.master.PASProductMapDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDO;
import com.metamorphosys.insureconnect.dataobjects.master.RuleDataDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyActivityDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientContactDetailsDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyClientDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageInsuredAccessoryDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyCoverageInsuredDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PolicyResponseDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.PremiumSIMappingTableDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.master.CityRepository;
import com.metamorphosys.insureconnect.jpa.master.CountryRepository;
import com.metamorphosys.insureconnect.jpa.master.LookupDataRepository;
import com.metamorphosys.insureconnect.jpa.master.PASProductMapRepository;
import com.metamorphosys.insureconnect.jpa.master.ProvinceRepository;
import com.metamorphosys.insureconnect.jpa.master.RuleRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntityDocumentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyActivityRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PolicyResponseRepository;
import com.metamorphosys.insureconnect.jpa.transaction.PremiumSIMapRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;
import com.metamorphosys.interfaces.tranferdataobjects.AccessoryTO;
import com.metamorphosys.interfaces.tranferdataobjects.CityTO;
import com.metamorphosys.interfaces.tranferdataobjects.CountryTO;
import com.metamorphosys.interfaces.tranferdataobjects.CoverageSumInsuredRatesTO;
import com.metamorphosys.interfaces.tranferdataobjects.CoverageTO;
import com.metamorphosys.interfaces.tranferdataobjects.CustomerInformationTO;
import com.metamorphosys.interfaces.tranferdataobjects.CustomerTO;
import com.metamorphosys.interfaces.tranferdataobjects.DetailListTO;
import com.metamorphosys.interfaces.tranferdataobjects.DetailMetadataTO;
import com.metamorphosys.interfaces.tranferdataobjects.NationalityTO;
import com.metamorphosys.interfaces.tranferdataobjects.OccupationTO;
import com.metamorphosys.interfaces.tranferdataobjects.PolicyTO;
import com.metamorphosys.interfaces.tranferdataobjects.ProductTO;
import com.metamorphosys.interfaces.tranferdataobjects.ProvinceTO;
import com.metamorphosys.interfaces.tranferdataobjects.QuotationTO;
import com.metamorphosys.interfaces.tranferdataobjects.RangeTO;
import com.metamorphosys.interfaces.tranferdataobjects.SubmitBindingResponseTO;
import com.metamorphosys.interfaces.tranferdataobjects.VehicleInformationTO;
import com.metamorphosys.interfaces.tranferdataobjects.VehiclePictureTO;
import com.metamorphosys.interfaces.tranferdataobjects.VehicleTO;


@RestController
@CrossOrigin
@RequestMapping("/interface/policyInterface")

public class PolicyInterface {

	private static final Logger log = LoggerFactory.getLogger(PolicyInterface.class);

	@Autowired
	PASProductMapRepository pasProductMapRepository;

	@Autowired
	CityRepository cityRepo;

	@Autowired
	ProvinceRepository provinceRepo;

	@Autowired
	PremiumSIMapRepository premiumSIMapRepository;

	@Autowired
	CountryRepository countryRepo;

	@Autowired
	LookupDataRepository lookupDataRepository;

	@Autowired
	ApplicationRepository applicationRepository;

	@Autowired
	EntityDocumentRepository entityDocRep;

	@Autowired
	RuleRepository ruleRepo;

	@Autowired
	PolicyActivityRepository policyActivityRepository;

	@Autowired
	PolicyResponseRepository policyResponseRepository;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> execute(@RequestBody String json, HttpServletRequest request){
		PolicyDO policyDO =(PolicyDO) SerializationUtility.getInstance().fromJson(json, PolicyDO.class);
		//		List<EntityDocumentDO> entityDocumentList = io.getEntityDocumentDOs();

		//submitBindingTO submitBindingTO = new submitBindingTO();

		PolicyCoverageDO baseplan = policyDO.getBasePlan();
		HashMap constantsHashMap = getPasMappindData(null,null);
		HashMap basePlanPAS = getPasMappindData(baseplan.getProductCd(),baseplan.getProductCd());
		//QuotationTO 
		PolicyTO policyTO = new PolicyTO();
		policyTO.setCommissionRate(policyDO.getCommissionRate());
		policyTO.setDiscountRate(policyDO.getDiscountRate());
		policyTO.setUserId(policyDO.getPolicyAgentList().get(0).getUserId());
		policyTO.setAgentUserId(policyDO.getPolicyAgentList().get(0).getUserId());
		//		policyTO.setUserId("bhavini@metamorphtech.com");
		//		policyTO.setPartnerId(policyDO.getAgentId());
		policyTO.setPartnerId(policyDO.getPolicyAgentList().get(0).getAgentId());
		//		policyTO.setReferenceId("VI-"+getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTID, baseplan.getProductCd(), basePlanPAS)+" "+convertTODate(policyDO.getPolicyCommencementDt())+"-"+" "+policyDO.getProposalNum());
		policyTO.setReferenceId(policyDO.getProposalNum());
		policyTO.setPolicyStartDate(convertTODate(policyDO.getPolicyCommencementDt()));
		policyTO.setPolicyEndDate(convertTODate(policyDO.getPolicyExpiryDt()));
		
		QuotationTO quotation = new QuotationTO();
		quotation.setSelectedIndex(0);
		//set base coverage map . ID 346 and 366 in pasProductTable
		quotation.setBasicCoverageType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.BASICCOVERAGETYPE, baseplan.getCoverageType(), constantsHashMap));
		quotation.setPolicyStartDate(convertTODate(policyDO.getPolicyCommencementDt()));
		quotation.setPolicyEndDate(convertTODate(policyDO.getPolicyExpiryDt()));
		quotation.setReferenceId(policyDO.getProposalNum());
		
		VehicleInformationTO vehicleInformationTO = new VehicleInformationTO();
		vehicleInformationTO.setKey(baseplan.getPolicyCoverageInsuredList().get(0).getVehicleKey());
		vehicleInformationTO.setText(baseplan.getPolicyCoverageInsuredList().get(0).getDerivedVehicleMakeCd() +" "+ baseplan.getPolicyCoverageInsuredList().get(0).getDerivedVehicleModelCd()+" " + baseplan.getPolicyCoverageInsuredList().get(0).getVehicleVariant());
		vehicleInformationTO.setSeat(baseplan.getPolicyCoverageInsuredList().get(0).getSeatingCapacity());
		vehicleInformationTO.setYearManufactured(baseplan.getPolicyCoverageInsuredList().get(0).getManufacturingDt());
		vehicleInformationTO.setSumInsured(String.format("%.0f",baseplan.getSumInsured()));
		vehicleInformationTO.setVehiclePrice(baseplan.getPolicyCoverageInsuredList().get(0).getDefaultSumInsured());
		vehicleInformationTO.setBrand(baseplan.getPolicyCoverageInsuredList().get(0).getDerivedVehicleMakeCd());
		vehicleInformationTO.setArea(getValuefromHashMap(InsureConnectConstants.ZONECD.ZONECD,baseplan.getPolicyCoverageInsuredList().get(0).getZoneCd(),constantsHashMap));
		quotation.setVehicleInformation(vehicleInformationTO);

		ProductTO productTO = new ProductTO();
		productTO.setProductId(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTID, baseplan.getProductCd(), basePlanPAS));
		productTO.setProductName(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTNAME, baseplan.getProductCd(), basePlanPAS));
		//productTO.setPrice(String.format("%.2f",policyDO.getFinalPremium()));
		productTO.setPrice(policyDO.getFinalPremium().toString());
		
		productTO.setProductType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.PRODUCTTYPE, baseplan.getProductCd(), basePlanPAS));
		productTO.setCurrency(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.CURRENCY, null, constantsHashMap));
		
		//get base plan rate 
		double basePlanRate=1;
		for(PolicyCoverageDO coverageDO:policyDO.getPolicyCoverageList()){
			if(coverageDO.getPlanTypeCd().equals("BASEPLAN")){
				basePlanRate=coverageDO.getPremiumRate();
				break;
			}
		}

		List<CoverageTO> coverageList = new ArrayList<CoverageTO>();
		int i=1;
		for(PolicyCoverageDO coverageDO:policyDO.getPolicyCoverageList()){
			CoverageTO coverageTO = new CoverageTO();
			HashMap coveragePASMap =null;
			if(InsureConnectConstants.PLANTYPE.RIDER.equals(coverageDO.getPlanTypeCd())){
				coveragePASMap =  getPasMappindData(coverageDO.getProductCd(),baseplan.getProductCd());
				coverageTO.setCoverageCode(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGECODE, coverageDO.getProductCd(), constantsHashMap));
				if(coverageDO.getAssociationType().equals("MANDATORY")){
					coverageTO.setCoverageType("Y");
				}else{
					coverageTO.setCoverageType("N");
				}
				coverageTO.setCoverageName(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGENAME, coverageDO.getProductCd(), constantsHashMap));
			}else{
				coveragePASMap= basePlanPAS;
				coverageTO.setCoverageCode(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGECODE, coverageDO.getCoverageType(), constantsHashMap));
				coverageTO.setCoverageType("B");
				coverageTO.setCoverageName(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGENAME, coverageDO.getCoverageType(), constantsHashMap));
			}

			//CoverageTO
			//			CoverageTO coverageTO = new CoverageTO();
			//			coverageTO.setCoverageType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGETYPE, null, coveragePASMap));
			//			coverageTO.setCoverageCode(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGECODE, coverageDO.getCoverageType(), coveragePASMap));
			//			coverageTO.setCoverageName(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGENAME, coverageDO.getCoverageType(), constantsHashMap));
			//			coverageTO.setCoverageGroup(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGEGROUP, null, coveragePASMap));
			coverageTO.setCoverageGroup(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.COVERAGEGROUP, coverageDO.getProductCd(), constantsHashMap));
			coverageTO.setDisplayOrder(i);
			i++;
			if((coverageDO.getPlanTypeCd().equals("RIDER")  && coverageDO.getAssociationType().equals("OPTIONAL"))){
				//				coverageTO.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE, null, coveragePASMap).equals("TRUE")?true:false);
				coverageTO.setEditable(true);
			}else{
				coverageTO.setEditable(false);
			}
			coverageTO.setSelectedIndex(0); // this might need to be changed afterwards based on the value selection of range or list
			//			coverageTO.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE, InsureConnectConstants.VALUETYPE.SELECTION, coveragePASMap));
			coverageTO.setValueType("boolean"); // every coverages own value type will always be boolean as it is slected or not

			List<String> list = new ArrayList<String>();
			//			if(coverageDO.isSelectedFlag()){
			//				list.add(InsureConnectConstants.BOOLEAN.TRUE);
			//			}else{
			//				list.add(InsureConnectConstants.BOOLEAN.FALSE);
			//			}

			if(null != coverageDO.getIsSelected() && coverageDO.getIsSelected().equals("true")){
				list.add(InsureConnectConstants.BOOLEAN.TRUE);
			}else{
				list.add(InsureConnectConstants.BOOLEAN.FALSE);
			}
			coverageTO.setValueList(list);
			coverageTO.setProrate((double) 1);

			//DetailMetadataTO
			DetailMetadataTO detailMetadataTO = new DetailMetadataTO();
			detailMetadataTO.setInsuredValue(0);
			detailMetadataTO.setPremiumRate(1);
			detailMetadataTO.setPremiumValue(2);
			detailMetadataTO.setDeductible(3);
			coverageTO.setDetailListMetadata(detailMetadataTO);

			//DetailListTO
			List<DetailListTO> detailListTOs = new ArrayList<DetailListTO>();
			// first detailstTO is equivalent of UI meta data for IDV of the car
			DetailListTO detailListTO = new DetailListTO();
			detailListTO.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE, InsureConnectConstants.EDITABLEVALUE.INSUREDVALUE, constantsHashMap).equals("TRUE")?true:false);
			//			detailListTO.setEditable(false);
			detailListTO.setSelectedIndex(0);
			//			detailListTO.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE, InsureConnectConstants.EDITABLEVALUE.INSUREDVALUE, coveragePASMap));
			detailListTO.setValueType(coverageDO.getCoverageUIProperty());
			if(detailListTO.getValueType().equals("range")){
				detailListTO.setRange(getRangeValue(coverageDO));
			};
			List<String> valList = new ArrayList<String>();
			//			coverageDO.getSumInsured().toString();
			String sumInsuredVal = String.format("%.0f", coverageDO.getSumInsured());
			
			
			
			valList.add(sumInsuredVal);
			//			detailListTO.setValueList(new ArrayList<String>(Arrays.asList(coverageDO.getSumInsured().toString())));
			detailListTO.setValueList(valList);
			detailListTO.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL, InsureConnectConstants.EDITABLEVALUE.INSUREDVALUE, constantsHashMap).equals("TRUE")?true:false);
			//			detailListTO.setDisplay(true);
			detailListTOs.add(detailListTO);

			// for premium rate section
			DetailListTO detailListTO2 = new DetailListTO();
			detailListTO2.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE, InsureConnectConstants.EDITABLEVALUE.PREMIUMRATE, constantsHashMap).equals("TRUE")?true:false);
			//			detailListTO2.setEditable(false);
			detailListTO2.setSelectedIndex(0);
			
			detailListTO2.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE, InsureConnectConstants.EDITABLEVALUE.PREMIUMRATE, constantsHashMap));
			//			detailListTO2.setValueType("percentage");
			//detailListTO2.setValueList(new ArrayList<String>(Arrays.asList(coverageDO.getPremiumRate().toString())));
			//detailListTO2.setValueList(new ArrayList<String>(Arrays.asList(String.valueOf(coverageDO.getPremiumRate()*100))));
			if(coverageDO.getProductCd().equals("AUTWRK")){
				detailListTO2.setValueList(new ArrayList<String>(Arrays.asList(String.valueOf(coverageDO.getPremiumRate()*basePlanRate*100))));
			}else if(coverageDO.getProductCd().equals("VPRKPT")){
				//either 0 or blank or 1
				detailListTO2.setValueList(new ArrayList<String>(Arrays.asList(String.valueOf(0))));
			}else {
				detailListTO2.setValueList(new ArrayList<String>(Arrays.asList(String.valueOf(coverageDO.getPremiumRate()*100))));
			}
			detailListTO2.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL, InsureConnectConstants.EDITABLEVALUE.PREMIUMRATE, constantsHashMap).equals("TRUE")?true:false);
			//detailListTO2.setDisplay(false);
			detailListTO2.setCoverageSumInsuredRates(getCoverageSumInsuredRate(coverageDO, baseplan, policyDO));

			//coverageSumInsuredRatesTO.se
			detailListTOs.add(detailListTO2);

			DetailListTO detailListTO3 = new DetailListTO();
			detailListTO3.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE, InsureConnectConstants.EDITABLEVALUE.PREMIUMVALUE, constantsHashMap).equals("TRUE")?true:false);
			//			detailListTO3.setEditable(false);
			detailListTO3.setSelectedIndex(0);
			detailListTO3.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE, InsureConnectConstants.EDITABLEVALUE.PREMIUMVALUE, constantsHashMap));
			//			detailListTO3.setValueType("currency");
			//--------------Decimal Issue Below-------------------
			//detailListTO3.setValueList(new ArrayList<String>(Arrays.asList(String.format("%.2f",coverageDO.getBaseAnnualPremium()))));
			detailListTO3.setValueList(new ArrayList<String>(Arrays.asList(coverageDO.getBaseAnnualPremium().toString())));
			//above line is changed because of decimal point issue in NUP changed by siddhant
			detailListTO3.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL, InsureConnectConstants.EDITABLEVALUE.PREMIUMVALUE, constantsHashMap).equals("TRUE")?true:false);
			//			detailListTO3.setDisplay(false);
			detailListTOs.add(detailListTO3);


			// for deductibles section
			DetailListTO detailListTO4 = new DetailListTO();
			detailListTO4.setEditable(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.ISEDITABLE, InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE, constantsHashMap).equals("TRUE")?true:false);
			//			detailListTO4.setEditable(false);
			detailListTO4.setSelectedIndex(0);
			detailListTO4.setValueType(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUETYPE, InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE, constantsHashMap));
			//			detailListTO4.setValueType("currency-desc");
			detailListTO4.setValueList(new ArrayList<String>(Arrays.asList(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.VALUELIST, coverageDO.getProductCd()+"-"+InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE, constantsHashMap))));
			detailListTO4.setDisplay(getValuefromHashMap(InsureConnectConstants.PASCONSTANTS.DISPLAYFL, InsureConnectConstants.EDITABLEVALUE.DEDUCTIBLE, constantsHashMap).equals("TRUE")?true:false);
			//			detailListTO4.setDisplay(true);
			detailListTOs.add(detailListTO4);


			coverageTO.setDetailList(detailListTOs);
			coverageList.add(coverageTO);
		}
		if(coverageList.size()>0){
			productTO.setCoverageList(coverageList);
		}
		quotation.setProduct(productTO);
		CustomerTO customerTO = new CustomerTO();
		for(PolicyClientDO policyClientDO:policyDO.getPolicyClientList()){
			if(InsureConnectConstants.ROLECD.PROPOSER.equals(policyClientDO.getRoleCd())){
				customerTO.setPartnerType(getValuefromHashMap(InsureConnectConstants.CLIENTTYPE.CLIENTCD,policyClientDO.getClientTypeCd(),constantsHashMap));
				customerTO.setFullName(policyClientDO.getFirstName());
				customerTO.setNameConfirmation(true);
				customerTO.setInstituteName(policyClientDO.getCompanyName());
				//				customerTO.setContactPersonName(policyClientDO.getContactPersonName());

				customerTO.setAddressLine1(policyClientDO.getPolicyClientAddressList().get(0).getAddressLine1());
				customerTO.setAddressLine2(policyClientDO.getPolicyClientAddressList().get(0).getAddressLine2());

				CityTO cityTO= new CityTO();
				cityTO.setCode(policyClientDO.getPolicyClientAddressList().get(0).getCityCd());
				cityTO.setName(cityRepo.findByCityCdAndProvinceCd(policyClientDO.getPolicyClientAddressList().get(0).getCityCd(), policyClientDO.getPolicyClientAddressList().get(0).getTalukaCd()).getCityName());
				customerTO.setCity(cityTO);

				ProvinceTO provinceTO = new ProvinceTO();
				provinceTO.setCode(policyClientDO.getPolicyClientAddressList().get(0).getTalukaCd());
				provinceTO.setName(provinceRepo.findByProvinceCd(policyClientDO.getPolicyClientAddressList().get(0).getTalukaCd()).getProvinceName());
				customerTO.setProvince(provinceTO);

				CountryTO countryTO = new CountryTO();
				countryTO.setCode(policyClientDO.getPolicyClientAddressList().get(0).getCountryCd());
				countryTO.setName(countryRepo.findByCountryCd(policyClientDO.getPolicyClientAddressList().get(0).getCountryCd()).getCountryDesc());
				customerTO.setCountry(countryTO);

				customerTO.setPostCode(policyClientDO.getPolicyClientAddressList().get(0).getPostCd().toString());
				if("P".equals(getValuefromHashMap(InsureConnectConstants.CLIENTTYPE.CLIENTCD,policyClientDO.getClientTypeCd(),constantsHashMap))){
					customerTO.setDob(convertTODate(policyClientDO.getDateOfBirth()));
					customerTO.setContactPersonName("");
					customerTO.setInstituteName("");
				}
				if("I".equals(getValuefromHashMap(InsureConnectConstants.CLIENTTYPE.CLIENTCD,policyClientDO.getClientTypeCd(),constantsHashMap))){
					customerTO.setIdNo("");
					customerTO.setContactPersonName(policyClientDO.getContactPersonName());
					customerTO.setFullName("");
				}
				customerTO.setGender(getValuefromHashMap(InsureConnectConstants.GENDERCD.GENDERCD,policyClientDO.getGenderCd(),constantsHashMap));
				for(PolicyClientContactDetailsDO policyClientContactDetailsDO:policyClientDO.getPolicyClientContactDetailsList()){
					if(InsureConnectConstants.CONTACTTYPE.MOBILE.equals(policyClientContactDetailsDO.getContactTypeCd())){
						customerTO.setMobileNo(policyClientContactDetailsDO==null?null:policyClientContactDetailsDO.getContactNum());
					}
					if(InsureConnectConstants.CONTACTTYPE.HOME.equals(policyClientContactDetailsDO.getContactTypeCd())){
						customerTO.setTelephoneNo(policyClientContactDetailsDO==null?null:policyClientContactDetailsDO.getContactNum());
					}
					if(InsureConnectConstants.CONTACTTYPE.EMAIL.equals(policyClientContactDetailsDO.getContactTypeCd())){
						customerTO.setEmailAddress(policyClientContactDetailsDO==null?null:policyClientContactDetailsDO.getEmailId());
					}
					if(InsureConnectConstants.CONTACTTYPE.OFFICE.equals(policyClientContactDetailsDO.getContactTypeCd())){
						customerTO.setTelephoneNo(policyClientContactDetailsDO==null?null:policyClientContactDetailsDO.getContactNum());
					}


				}

				customerTO.setIdNo(policyClientDO.getIdentityNum());
				List<EntityDocumentDO> documentDOs = entityDocRep.findByEntityIDAndDocumentTypeCd(policyDO.getPolicyClientList().get(0).getOfflineGuid(), InsureConnectConstants.DOCUMENTYPECD.IDPROOF);
				//				List<EntityDocumentDO> documentDOs = entityDocumentList;
				NationalityTO nationalityTO = new NationalityTO();
				
				if(policyClientDO != null)
				{
					
					if(policyClientDO.getIdentityTypeCD() != null)
					{
						nationalityTO.setCode(policyClientDO.getIdentityTypeCD());
						nationalityTO.setName(lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.LOOKUPCD.IDENTITYTYPE, policyClientDO.getIdentityTypeCD()).getLookupValue());
						if(policyClientDO.getIdentityTypeCD().equals("PASPOR")) {
							nationalityTO.setCode("PASSPORT");
							nationalityTO.setName("PASSPORT");
						}
						//above code added as advice from Fabian that we have to fix this temperorily
						customerTO.setNationalIdType(nationalityTO);
					}
				}

				
				if(documentDOs.size()>0){

					if(documentDOs.get(0)!=null)
					{
						if(documentDOs.get(0).getReferenceID()!=null)
						{
							customerTO.setNationalIdTypeImageId(Integer.valueOf(documentDOs.get(0).getReferenceID()));
						}

						if(documentDOs.get(0).getEntityImageName()!=null)
						{
							customerTO.setNationalIdTypeImageName(documentDOs.get(0).getEntityImageName());
						}
					}


				}

				OccupationTO occupationTO = new OccupationTO();
				if(policyClientDO != null)
				{
				//	String firstName = policyClientDO.getFirstName();
					String occupation = policyClientDO.getOccupationCd();
					//code to handle null value by Bhavini 07/04/2017
					if(occupation == null ){
						occupationTO.setCode("");
						occupationTO.setName("");
					}else{
						occupationTO.setCode(occupation);
						occupationTO.setName(lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.LOOKUPCD.OCCUPATION, occupation).getLookupValue());
					}
					customerTO.setOccupation(occupationTO);
				}
				policyTO.setCustomer(customerTO);

				//CustomerInformationTO
				CustomerInformationTO customerInformationTO= new CustomerInformationTO();
				customerInformationTO.setName(policyClientDO.getFirstName());
				quotation.setCustomerInformationTO(customerInformationTO);
				break;
			}
		}
		VehicleTO vehicleTO = new VehicleTO();
		vehicleTO.setEngineNo(baseplan.getPolicyCoverageInsuredList().get(0).getEngineNum());
		//Adding purchase Date
		try {
			int MILLIS_IN_DAY = 1000 * 60 * 60 * 24;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String vehiclePurchaseDate=dateFormat.format(baseplan.getPolicyCoverageInsuredList().get(0).getPurchaseDt().getTime()+MILLIS_IN_DAY);
			vehicleTO.setVehiclePurchaseDate(vehiclePurchaseDate);
		}catch(Exception e){
			
		}
		
		//Purchase date added
		vehicleTO.setChassisNo(baseplan.getPolicyCoverageInsuredList().get(0).getChasisNum());
		
		//		vehicleTO.setPlateNo(baseplan.getPolicyCoverageInsuredList().get(0).getRegistrationNum());
		if(null != baseplan.getPolicyCoverageInsuredList().get(0).getMileage()){
			vehicleTO.setMileage(String.valueOf(baseplan.getPolicyCoverageInsuredList().get(0).getMileage().intValue()));
		}else{
			vehicleTO.setMileage("");
		}
		vehicleTO.setVehicleUsage(baseplan.getPolicyCoverageInsuredList().get(0).getUsagePurpose());
		vehicleTO.setStartDate(convertTODate(policyDO.getPolicyCommencementDt()));
		vehicleTO.setEndDate(convertTODate(policyDO.getPolicyExpiryDt()));

		List<EntityDocumentDO> documentDOs = entityDocRep.findByEntityIDAndDocumentTypeCd(baseplan.getPolicyCoverageInsuredList().get(0).getOfflineGuid(),InsureConnectConstants.DOCUMENTYPECD.VEHICLE);
		//		List<EntityDocumentDO> documentDOs = entityDocumentList;
		VehiclePictureTO pictureTO = new VehiclePictureTO();
		for(EntityDocumentDO entityDocumentDO:documentDOs){
			// first check whether entityImage null or not. If not null then only proceed 
			if(entityDocumentDO.getEntityImage() != null){
				Integer imageReferenceId = Integer.valueOf(entityDocumentDO.getReferenceID());
				if(InsureConnectConstants.DOCSUBTYPECD.FRONTLEFT.equals(entityDocumentDO.getDocumentSubTypeCd())){
					pictureTO.setFrontLeft(entityDocumentDO.getEntityImageName());
					pictureTO.setFrontLeftImageId(imageReferenceId);
				}
				if(InsureConnectConstants.DOCSUBTYPECD.FRONTRIGHT.equals(entityDocumentDO.getDocumentSubTypeCd())){
					pictureTO.setFrontRight(entityDocumentDO.getEntityImageName());
					pictureTO.setFrontRightImageId(imageReferenceId);
				}
				if(InsureConnectConstants.DOCSUBTYPECD.BACKLEFT.equals(entityDocumentDO.getDocumentSubTypeCd())){
					pictureTO.setBackLeft(entityDocumentDO.getEntityImageName());
					pictureTO.setBackLeftImageId(imageReferenceId);
				}
				if(InsureConnectConstants.DOCSUBTYPECD.BACKRIGHT.equals(entityDocumentDO.getDocumentSubTypeCd())){
					pictureTO.setBackRight(entityDocumentDO.getEntityImageName());
					pictureTO.setBackRightImageId(imageReferenceId);
				}
				if(InsureConnectConstants.DOCSUBTYPECD.CHASIS.equals(entityDocumentDO.getDocumentSubTypeCd())){
					pictureTO.setChassisNo(entityDocumentDO.getEntityImageName());
					pictureTO.setChassisNoImageId(imageReferenceId);
				}
			}
		}
		EntityDocumentDO vehicleIdentityImage = entityDocRep.findByEntityIDAndDocumentTypeCd(baseplan.getPolicyCoverageInsuredList().get(0).getOfflineGuid(),InsureConnectConstants.DOCSUBTYPECD.IDENTITY).get(0);
		Integer vehicleImageReferenceId = Integer.valueOf(vehicleIdentityImage.getReferenceID());
		if(InsureConnectConstants.DOCSUBTYPECD.IDENTITY.equals(vehicleIdentityImage.getDocumentTypeCd())){
			vehicleTO.setVehicleDocumentTypeImageId(vehicleImageReferenceId);
			vehicleTO.setVehicleDocumentType(vehicleIdentityImage.getDocumentSubTypeCd());
			if("STNK".equals(vehicleIdentityImage.getDocumentSubTypeCd())){
				vehicleTO.setPlateNo(baseplan.getPolicyCoverageInsuredList().get(0).getRegistrationNum());
			}else{
				vehicleTO.setPlateNo("");
			}
			vehicleTO.setVehicleDocumentTypeImageName(vehicleIdentityImage.getEntityImageName());
		}
		vehicleTO.setVehiclePicture(pictureTO);
		List<AccessoryTO> accessoryTOs = new ArrayList<AccessoryTO>();
		List<EntityDocumentDO> accessoryDOs = entityDocRep.findByEntityIDAndDocumentTypeCd(baseplan.getPolicyCoverageInsuredList().get(0).getOfflineGuid(), InsureConnectConstants.DOCUMENTYPECD.ACCESSORYIMAGE);
		for(PolicyCoverageInsuredAccessoryDO coverageInsuredAccessoryDO:baseplan.getPolicyCoverageInsuredList().get(0).getPolicyCoverageInsuredAccessoryList()){
			if(accessoryDOs!=null){
				boolean imageUploaded=false;
				for(EntityDocumentDO accessoryEntity:accessoryDOs){
					if(coverageInsuredAccessoryDO.getAccessoryCd().equals(accessoryEntity.getDocumentSubTypeCd())){
						AccessoryTO accessoryTO= new AccessoryTO();
						accessoryTO.setAccessoryImageName(accessoryEntity.getEntityImageName());
						accessoryTO.setAccessoryImageId(accessoryEntity.getReferenceID());
						accessoryTO.setAccessoryCode(coverageInsuredAccessoryDO.getAccessoryCd());
						accessoryTO.setAccessoryAmount(coverageInsuredAccessoryDO.getAccessoryValue());
						accessoryTO.setAccessoryDesc(coverageInsuredAccessoryDO.getDescription());
						accessoryTO.setAccessoryName(lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.LOOKUPCD.ACCESSORY, coverageInsuredAccessoryDO.getAccessoryCd()).getLookupValue());
						accessoryTOs.add(accessoryTO);
						imageUploaded=true;
					}
				}
				if(!imageUploaded){
					AccessoryTO accessoryTO= new AccessoryTO();
					accessoryTO.setAccessoryCode(coverageInsuredAccessoryDO.getAccessoryCd());
					accessoryTO.setAccessoryAmount(coverageInsuredAccessoryDO.getAccessoryValue());
					accessoryTO.setAccessoryDesc(coverageInsuredAccessoryDO.getDescription());
					accessoryTO.setAccessoryName(lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.LOOKUPCD.ACCESSORY, coverageInsuredAccessoryDO.getAccessoryCd()).getLookupValue());
					accessoryTOs.add(accessoryTO);
				}
			}else{
					AccessoryTO accessoryTO= new AccessoryTO();
					accessoryTO.setAccessoryCode(coverageInsuredAccessoryDO.getAccessoryCd());
					accessoryTO.setAccessoryAmount(coverageInsuredAccessoryDO.getAccessoryValue());
					accessoryTO.setAccessoryDesc(coverageInsuredAccessoryDO.getDescription());
					accessoryTO.setAccessoryName(lookupDataRepository.findByLookupCdAndLookupKey(InsureConnectConstants.LOOKUPCD.ACCESSORY, coverageInsuredAccessoryDO.getAccessoryCd()).getLookupValue());
					accessoryTOs.add(accessoryTO);
			}
		}
		vehicleTO.setAccessoryList(accessoryTOs);
		
		policyTO.setVehicle(vehicleTO);
		policyTO.setQuotation(quotation);

		//		submitBindingTO.setQuotation(quotation);
		String url=applicationRepository.findByKey(InsureConnectConstants.Path.POLICYBINDINGINTERFACEPATHDPATH).getValue();//"https://sit.utamaportal.azindo.allianz.co.id/sit/binding-api/1.0/submit?language=in_ID&channel=web";

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		httpHeaders.set("Authorization", request.getHeader("Authorization"));
		httpHeaders.set("channel", "mobile");

		//		UriComponentsBuilder queryParamBuilder = UriComponentsBuilder.fromHttpUrl(url);
		//		queryParamBuilder.queryParam("language", "in_ID");
		//		queryParamBuilder.queryParam("channel", "web");

		//		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		//		map.add("language", "in_ID");
		//		map.add("channel", "web");

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
		HttpEntity<PolicyTO> entity = new HttpEntity<PolicyTO>(policyTO, httpHeaders);


		//		String JsonToBeSent = "{\"userId\":\"bhavini@metamorphtech.com\",\"partnerId\":\"1775661\",\"referenceId\":\"266\",\"policyStartDate\":\"2017-02-11\",\"policyEndDate\":\"2018-02-11\",\"quotation\":{\"selectedIndex\":0,\"basicCoverageType\":\"COMPRE\",\"referenceId\":\"266\",\"policyStartDate\":\"2017-02-11\",\"policyEndDate\":\"2018-02-11\",\"vehicleInformation\":{\"key\":\"FORFIE059\",\"text\":\"HONDA Accord 3.5V6\",\"brand\":\"HONDA\",\"area\":\"2\",\"seat\":5,\"sumInsured\":\"708000000\",\"yearManufactured\":2015},\"product\":{\"productId\":\"1936\",\"productName\":\"MOBILKU NON PACKAGE\",\"currency\":\"IDR\",\"price\":\"\",\"productType\":\"NON-PKG\",\"coverageList\":[{\"coverageCode\":\"COMPRE\",\"coverageName\":\"Komprehensif\",\"coverageType\":\"B\",\"displayOrder\":1,\"isEditable\":false,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"B\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.0126\"],\"display\":false,\"coverageSumInsuredRates\":[]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"8920800\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"FLOOD-WH\",\"coverageName\":\"Badai, Hujan Es, Banjir, Genangan, Tanah Longsor dan Benturan Air\",\"coverageType\":\"Y\",\"displayOrder\":2,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.001\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":false}]},{\"coverageCode\":\"EQVET\",\"coverageName\":\"Gempa Bumi, Letusan Gunung Berapi, dan Tsunami\",\"coverageType\":\"Y\",\"displayOrder\":3,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.001\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":false}]},{\"coverageCode\":\"SRCC\",\"coverageName\":\"Pemogokan, Kerusuhan, dan Huru-hara\",\"coverageType\":\"Y\",\"displayOrder\":4,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"5.0E-4\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"354000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":false}]},{\"coverageCode\":\"TS\",\"coverageName\":\"Terorisme dan Sabotase\",\"coverageType\":\"Y\",\"displayOrder\":5,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"5.0E-4\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"354000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":false}]},{\"coverageCode\":\"TPL\",\"coverageName\":\"Tanggung Jawab Hukum Terhadap Pihak Ketiga\",\"coverageType\":\"Y\",\"displayOrder\":6,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"range\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"51000000\"],\"display\":true,\"range\":{\"min\":\"0\",\"max\":\"100000000\"}},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.005\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"25000000\",\"premiumRate\":\"1\"},{\"sumInsuredFrom\":\"25000001\",\"sumInsuredTo\":\"50000000\",\"premiumRate\":\"0.75\"},{\"sumInsuredFrom\":\"50000001\",\"sumInsuredTo\":\"100000000\",\"premiumRate\":\"0.5\"},{\"sumInsuredFrom\":\"100000001\",\"sumInsuredTo\":\"999999999\",\"premiumRate\":\"0.4\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"255000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"PLL\",\"coverageName\":\"Tanggung Jawab Hukum Terhadap Penumpang\",\"coverageType\":\"Y\",\"displayOrder\":7,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"range\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"25000000\"],\"display\":true,\"range\":{\"min\":\"0\",\"max\":\"100000000\"}},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.005\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"25000000\",\"premiumRate\":\"0.5\"},{\"sumInsuredFrom\":\"25000001\",\"sumInsuredTo\":\"50000000\",\"premiumRate\":\"0.375\"},{\"sumInsuredFrom\":\"50000001\",\"sumInsuredTo\":\"100000000\",\"premiumRate\":\"0.25\"},{\"sumInsuredFrom\":\"100000001\",\"sumInsuredTo\":\"999999999\",\"premiumRate\":\"0.23\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"125000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"PA-D\",\"coverageName\":\"Kecelakaan Diri Pengemudi\",\"coverageType\":\"Y\",\"displayOrder\":8,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"range\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"5000000\"],\"display\":true,\"range\":{\"min\":\"0\",\"max\":\"50000000\"}},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.005\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"25000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"PA-P\",\"coverageName\":\"Kecelakaan Diri Penumpang\",\"coverageType\":\"Y\",\"displayOrder\":9,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"range\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"5000000\"],\"display\":true,\"range\":{\"min\":\"0\",\"max\":\"50000000\"}},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.001\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"20000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"ME\",\"coverageName\":\"Biaya Medis\",\"coverageType\":\"Y\",\"displayOrder\":10,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"2500000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"3.5000000000000005E-4\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"875\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"PE\",\"coverageName\":\"Kehilangan Barang Pribadi\",\"coverageType\":\"Y\",\"displayOrder\":11,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"list\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"2000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.01\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"20000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"AMBSER\",\"coverageName\":\"Layanan Mobil Ambulan\",\"coverageType\":\"Y\",\"displayOrder\":12,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"list\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"500000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"2.0E-4\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"100\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"TXA\",\"coverageName\":\"Tunjangan Biaya Taksi\",\"coverageType\":\"Y\",\"displayOrder\":13,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"500000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.35\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"175000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"ERA\",\"coverageName\":\"Layanan Bantuan Darurat di Jalan Raya\",\"coverageType\":\"Y\",\"displayOrder\":14,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"5.0E-6\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"3540\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"AUTHORIZED\",\"coverageName\":\"Perbaikan di Bengkel Resmi\",\"coverageType\":\"Y\",\"displayOrder\":15,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.1\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"892080\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"CASH\",\"coverageName\":\"Santunan Tunai\",\"coverageType\":\"Y\",\"displayOrder\":16,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"1000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.0\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"MOBILE\",\"coverageName\":\"Inspeksi di tempat\",\"coverageType\":\"Y\",\"displayOrder\":17,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.0\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"THEFT\",\"coverageName\":\"Pencurian oleh Pengemudi\",\"coverageType\":\"Y\",\"displayOrder\":18,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"8.999999999999999E-5\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"63720\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"VALET\",\"coverageName\":\"Perlindungan Layanan Parkir Valet\",\"coverageType\":\"Y\",\"displayOrder\":19,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"coverageGroup\":\"Y\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"708000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"1.0\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"70000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]},{\"coverageCode\":\"PA24HR\",\"coverageName\":\"PA24 HR\",\"displayOrder\":20,\"isEditable\":true,\"selectedIndex\":0,\"valueType\":\"boolean\",\"prorate\":1.0,\"valueList\":[\"true\"],\"detailListMetadata\":{\"insuredValue\":0,\"premiumRate\":1,\"premiumValue\":2,\"deductible\":3},\"detailList\":[{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"100000000\"],\"display\":true},{\"valueType\":\"percentage\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"0.001\"],\"display\":false,\"coverageSumInsuredRates\":[{\"sumInsuredFrom\":\"0\",\"sumInsuredTo\":\"1000000000\"}]},{\"valueType\":\"currency\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[\"100000\"],\"display\":false},{\"valueType\":\"currency-desc\",\"selectedIndex\":0,\"isEditable\":false,\"valueList\":[null],\"display\":true}]}]},\"customerInformationTO\":{\"name\":\"Swapniul\"}},\"customer\":{\"partnerType\":\"individual\",\"fullName\":\"Swapniul\",\"dob\":\"2005-03-18\",\"gender\":\"male\",\"addressLine1\":\"sdsads\",\"addressLine2\":\"sadasd\",\"postCode\":\"10010\",\"mobileNo\":213213213,\"telephoneNo\":233243,\"emailAddress\":\"amit@metamorphtech.com\",\"idNo\":\"21321321\",\"nationalIdTypeImageName\":\"PERSONAL_ID-PERSONAL_ID_20170211_null-20170216-212745387.png\",\"nationalIdTypeImageId\":2152,\"nameConfirmation\":true,\"occupation\":{\"code\":\"MGR\",\"name\":\"Manager\"},\"city\":{\"code\":\"dff\",\"name\":\"Jakarta\"},\"province\":{\"code\":\"sfdsfsf\",\"name\":\"Jakarta\"},\"country\":{\"code\":\"INA\",\"name\":\"INDONESIA\"},\"nationalIdType\":{\"code\":\"KITAS\",\"name\":\"KITAS\"}},\"vehicle\":{\"engineNo\":\"3242143\",\"chassisNo\":\"23213\",\"plateNo\":\"B12SD1234\",\"mileage\":6.0,\"vehicleUsage\":\"P0\",\"vehicleDocumentType\":\"VEHICLE\",\"vehicleDocumentTypeImageId\":2204,\"startDate\":\"2017-02-11\",\"endDate\":\"2018-02-11\",\"vehicleDocumentTypeImageName\":\"IDENTITY-IDENTITY_20170211_null-20170217-14225517.png\",\"vehiclePicture\":{\"backLeft\":\"VEHICLE_BACK_LEFT-VEHICLE_BACK_LEFT_20170211_null-20170217-142247522.png\",\"backLeftImageId\":2199,\"backRight\":\"VEHICLE_BACK_RIGHT-VEHICLE_BACK_RIGHT_20170211_null-20170217-142248379.png\",\"backRightImageId\":2200,\"chassisNo\":\"VEHICLE_CHASSIS_NO-VEHICLE_CHASSIS_NO_20170211_null-20170217-142251898.png\",\"chassisNoImageId\":2201,\"frontLeft\":\"VEHICLE_FRONT_LEFT-VEHICLE_FRONT_LEFT_20170211_null-20170217-142253402.png\",\"frontLeftImageId\":2202,\"frontRight\":\"VEHICLE_FRONT_RIGHT-VEHICLE_FRONT_RIGHT_20170211_null-20170217-142254200.png\",\"frontRightImageId\":2203},\"accessoryList\":[]}}";
		//	map.add("jsonRequest", JsonToBeSent);
		//queryParamBuilder.queryParam("requestId", request.getHeader("requestId"));
		//		HttpEntity<String> entity = new HttpEntity<String>(SerializationUtility.getInstance().toJson(policyTO), httpHeaders);
		//	HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<MultiValueMap<String, String>>(map, httpHeaders);
		PolicyActivityDO policyActivityDO= new PolicyActivityDO();
		policyActivityDO.setActivityKey("submit-binding-string-"+policyDO.getProposalRefNum());
		policyActivityDO.setActivityValue(SerializationUtility.getInstance().toJson(policyTO));
		policyActivityDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
		policyActivityDO.setActivityStatus(InsureConnectConstants.Status.PENDING);
		policyActivityRepository.save(policyActivityDO);
		ResponseEntity<String> jsonObject = restTemplate.exchange(url, 
				HttpMethod.POST, entity, String.class);
		//		ResponseEntity<String> jsonObject = restTemplate.exchange(queryParamBuilder.build().encode().toUri(),
		//				HttpMethod.POST, entity, String.class);
		log.info("RESPONSE From NUP");
		log.info(jsonObject.getBody());



		PolicyResponseDO policyResponseDO=new PolicyResponseDO();
		policyResponseDO.setActivityKey("submit-binding-response-"+policyDO.getProposalRefNum());
		policyResponseDO.setActivityValue(jsonObject.getBody());
		policyResponseDO.setSystemCreatedDt(new Timestamp(System.currentTimeMillis()));
		SubmitBindingResponseTO response = (SubmitBindingResponseTO) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), SubmitBindingResponseTO.class);
		if(response.getResponseCode() == 200 && response.getStatusCode().equals("BS000")){
			policyResponseDO.setActivityStatus(response.getStatusCode()+"-PASSED");
		}else{
			policyResponseDO.setActivityStatus(response.getStatusCode()+"-FAILED");
		}
		policyResponseRepository.save(policyResponseDO);

		HashMap <String, String> submitBindingResultHashMap = new HashMap<String, String>();
		log.info("Response Code : "+response.getResponseCode());
		if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
			submitBindingResultHashMap.put("submitBindingStatus", "SERVER_ERROR");
			return new ResponseEntity(submitBindingResultHashMap,jsonObject.getStatusCode());
		}else{
			SubmitBindingResponseTO res = (SubmitBindingResponseTO) SerializationUtility.getInstance().fromJson(jsonObject.getBody(), SubmitBindingResponseTO.class);
			if(res.getResponseCode() == 200 && res.getStatusCode().equals("BS000")){
				submitBindingResultHashMap.put("submitBindingStatus", "PASSED");
				return new ResponseEntity(submitBindingResultHashMap, HttpStatus.OK);
			}else{
				submitBindingResultHashMap.put("submitBindingStatus", "FAILED");
				return new ResponseEntity(submitBindingResultHashMap, HttpStatus.OK);
			}
			//			submitBindingResultHashMap.put("submitBindingResult", jsonObject.getBody());
			//			return new ResponseEntity(submitBindingResultHashMap, HttpStatus.OK);
		}
		//		return jsonObject.getBody();		
	}

	public HashMap getPasMappindData(String productCd, String basePlanCd){
		List<PASProductMapDO> pasProductMapDOs = pasProductMapRepository.findByProductCdAndBasePlanCd(productCd, basePlanCd);
		HashMap pasHashMap= new HashMap();
		for(PASProductMapDO pasProductMapDO:pasProductMapDOs){
			String key =pasProductMapDO.getFieldKey().trim();
			if(pasProductMapDO.getSystemValue()!=null){
				key = key + pasProductMapDO.getSystemValue();
			}

			pasHashMap.put(key, pasProductMapDO.getExternalValue());
		}
		return pasHashMap;
	}

	public String getValuefromHashMap(String fieldKey,String systemValue , HashMap map){
		String key = fieldKey.trim();
		if(systemValue != null){
			key = key + systemValue;
		}

		String data = (String) map.get(key);
		return data;	
	}

	public List<CoverageSumInsuredRatesTO> getCoverageSumInsuredRateOld(PolicyCoverageDO coverageDO, PolicyCoverageDO basePlan, PolicyDO policyDO){
		List<CoverageSumInsuredRatesTO> coverageSumInsuredRatesTOs = new ArrayList<CoverageSumInsuredRatesTO>();
		RangeTO rangeTO = new RangeTO();
		if("FLWTHM".equals(coverageDO.getProductCd()) || "EAVTEM".equals(coverageDO.getProductCd()) || 
				"STRTCM".equals(coverageDO.getProductCd()) || "TERRSM".equals(coverageDO.getProductCd()) || 
				"PADRVR".equals(coverageDO.getProductCd()) || "PAPSGR".equals(coverageDO.getProductCd())
				|| "TAXALL".equals(coverageDO.getProductCd()) || "EERRAA".equals(coverageDO.getProductCd())
				|| "AMBSRV".equals(coverageDO.getProductCd()) || "THBDRV".equals(coverageDO.getProductCd())
				|| "PEREFF".equals(coverageDO.getProductCd()) || "PA24HR".equals(coverageDO.getProductCd())){
			//			coverageSumInsuredRatesTO.setSumInsuredFrom((double) 0);
			//			coverageSumInsuredRatesTO.setSumInsuredTo((double) 1000000000);
			//			coverageSumInsuredRatesTO.setPremiumRate(coverageDO.getPremiumRate());
			//			coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
			rangeTO.setMin("0");
			rangeTO.setMax("1000000000");
		}else if("THPRLB".equals(coverageDO.getProductCd()) || "PRLGLB".equals(coverageDO.getProductCd())
				|| "MEDEXP".equals(coverageDO.getProductCd()) || "CSHSTM".equals(coverageDO.getProductCd()) 
				|| "MOBIMS".equals(coverageDO.getProductCd()) || "VPRKPT".equals(coverageDO.getProductCd()) ){
			List<PremiumSIMappingTableDO> mappingTableDOs= premiumSIMapRepository.findByProductCd(coverageDO.getProductCd());
			for(PremiumSIMappingTableDO premiumSIMappingTableDO:mappingTableDOs){
				CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
				coverageSumInsuredRatesTO.setSumInsuredFrom(String.format("%.0f", premiumSIMappingTableDO.getSIFrom()));
				coverageSumInsuredRatesTO.setSumInsuredTo(String.format("%.0f",premiumSIMappingTableDO.getSITo()));
				coverageSumInsuredRatesTO.setPremiumRate(String.format("%.0f", premiumSIMappingTableDO.getPremiumRate()));
				coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
			}
		}else if("AUTWRK".equals(coverageDO.getProductCd())){
			CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
			coverageSumInsuredRatesTO.setSumInsuredFrom(String.format("%.0f",  0));
			coverageSumInsuredRatesTO.setSumInsuredTo(String.format("%.0f",  1000000000));
			coverageSumInsuredRatesTO.setPremiumRate(String.format("%.0f", coverageDO.getPremiumRate()*basePlan.getPremiumRate()));
			coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
		}
		//	COMMENTED RULEREPOSITORY METHOD ISSUE
		else if("MOBECO".equals(coverageDO.getProductCd()) || "MOBGRD".equals(coverageDO.getProductCd())
				|| "NONPKG".equals(coverageDO.getProductCd())){
			List<RuleDO>  ruleDO1 = ruleRepo.findByRuleNameAndDate("BaseCoverRateTable", policyDO.getPolicyCommencementDt());
			//			RuleDO ruleDO1 = ruleRepo.findByRuleName("BaseCoverRateTable");
			PolicyCoverageInsuredDO vehicle = coverageDO.getPolicyCoverageInsuredList().get(0);
			for(RuleDataDO do1:ruleDO1.get(0).getRuleDataList()){
			//	Double entryFrom = Double.parseDouble(do1.getCondition4());
				//Double entryTo = Double.parseDouble(do1.getCondition5());
				//if(entryFrom<=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() && entryTo>=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() ){
				if(Double.parseDouble(do1.getCondition4()) <= vehicle.getEntryAge() && Double.parseDouble(do1.getCondition5()) >= vehicle.getEntryAge() && do1.getCondition3().equals(vehicle.getZoneCd()) && do1.getCondition6().equals(vehicle.getVehicleMakeCd()) && do1.getCondition7().equals(vehicle.getVehicleModelCd()) && do1.getCondition8().equals(basePlan.getCoverageType())){
					CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
					coverageSumInsuredRatesTO.setSumInsuredFrom(String.format("%.0f", do1.getCondition1()));
					coverageSumInsuredRatesTO.setSumInsuredTo(String.format("%.0f", do1.getCondition2()));
					coverageSumInsuredRatesTO.setPremiumRate(String.format("%.0f", do1.getAction1()));
					coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
				}
			}
			//do nothing by swapnil 2nd feb 2017
		}

		return coverageSumInsuredRatesTOs;
	}

	public List<CoverageSumInsuredRatesTO> getCoverageSumInsuredRate(PolicyCoverageDO coverageDO, PolicyCoverageDO basePlan, PolicyDO policyDO){
		List<CoverageSumInsuredRatesTO> coverageSumInsuredRatesTOs = new ArrayList<CoverageSumInsuredRatesTO>();
		if(coverageDO.getPlanTypeCd().equals(InsureConnectConstants.PLANTYPE.BASEPLAN)){
			List<RuleDO>  ruleDO1 = ruleRepo.findByRuleNameAndDate("BaseCoverRateTable", policyDO.getPolicyCommencementDt());
			//			RuleDO ruleDO1 = ruleRepo.findByRuleName("BaseCoverRateTable");
			PolicyCoverageInsuredDO vehicle = basePlan.getPolicyCoverageInsuredList().get(0);

			for(RuleDataDO do1:ruleDO1.get(0).getRuleDataList()){
				//if(entryFrom<=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() && entryTo>=basePlan.getPolicyCoverageInsuredList().get(0).getEntryAge() ){
				if(Double.parseDouble(do1.getCondition4()) <= vehicle.getEntryAge() && Double.parseDouble(do1.getCondition5()) >= vehicle.getEntryAge() && do1.getCondition3().equals(vehicle.getZoneCd()) && do1.getCondition6().equals(vehicle.getVehicleMakeCd()) && do1.getCondition7().equals(vehicle.getVehicleModelCd()) && do1.getCondition8().equals(basePlan.getCoverageType())){
					CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
					coverageSumInsuredRatesTO.setSumInsuredFrom(do1.getCondition1());
					coverageSumInsuredRatesTO.setSumInsuredTo(do1.getCondition2());
					coverageSumInsuredRatesTO.setPremiumRate(do1.getAction1());
					coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
				}
			}
			//do nothing by swapnil 2nd feb 2017
		}
		if(coverageDO.getPlanTypeCd().equals(InsureConnectConstants.PLANTYPE.RIDER)){
			if(coverageDO.getCoverageUIProperty().equals("list")){
				CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
				coverageSumInsuredRatesTO.setSumInsuredFrom("0");
				coverageSumInsuredRatesTO.setSumInsuredTo("1000000000");
				coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);

			}
			if(coverageDO.getCoverageUIProperty().equals("range")){
				if(coverageDO.getProductCd().equals("PADRVR") || coverageDO.getProductCd().equals("PAPSGR")){
					CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
					coverageSumInsuredRatesTO.setSumInsuredFrom("0");
					coverageSumInsuredRatesTO.setSumInsuredTo("1000000000");
					coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
				}else{
					//List<RuleDataDO> sumInsuredList = ruleRepo.findByRuleNameAndDate(ruleRepo.findByRuleNameAndDate(basePlan.getProductCd()+coverageDO.getProductCd()+"PREMIUMRATE", policyDO.getPolicyCommencementDt()).get(0).getRuleDataList().get(0).getAction1(),policyDO.getPolicyCommencementDt()).get(0).getRuleDataList();
					List<RuleDO> firstRule = ruleRepo.findByRuleNameAndDate(basePlan.getProductCd()+coverageDO.getProductCd()+"PREMIUMRATE", policyDO.getPolicyCommencementDt());
					RuleDataDO dataOfFirstRule = firstRule.get(0).getRuleDataList().get(0);
					String nexdtRuleName = dataOfFirstRule.getAction1();
					List<RuleDO> secondRule = ruleRepo.findByRuleNameAndDate(nexdtRuleName, policyDO.getPolicyCommencementDt());
					List<RuleDataDO> dataOfSecondRule = secondRule.get(0).getRuleDataList();
					for(RuleDataDO ruleDataDO:dataOfSecondRule){
						CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
						coverageSumInsuredRatesTO.setSumInsuredFrom(ruleDataDO.getCondition1());
						coverageSumInsuredRatesTO.setSumInsuredTo(ruleDataDO.getCondition2());
						coverageSumInsuredRatesTO.setPremiumRate(ruleDataDO.getAction1());
						coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
					}
				}
			}
			if(coverageDO.getCoverageUIProperty().equals("currency")){
				CoverageSumInsuredRatesTO coverageSumInsuredRatesTO = new CoverageSumInsuredRatesTO();
				coverageSumInsuredRatesTO.setSumInsuredFrom("0");
				coverageSumInsuredRatesTO.setSumInsuredTo("1000000000");
				coverageSumInsuredRatesTOs.add(coverageSumInsuredRatesTO);
			}
		}

		return coverageSumInsuredRatesTOs;
	}

	public String convertTODate(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String format = formatter.format(date);
		return format;
	}

	public RangeTO getRangeValue(PolicyCoverageDO coverageDO){
		RangeTO range = new RangeTO();

		if(coverageDO.getCoverageUIPropertyValue().contains("-"))
		{
			range.setMin(coverageDO.getCoverageUIPropertyValue().split("-")[1]);
			range.setMax(coverageDO.getCoverageUIPropertyValue().split("-")[2]);
		}else{
			range.setMin(coverageDO.getCoverageUIPropertyValue());
			range.setMax(coverageDO.getCoverageUIPropertyValue());
		}

		return range;
	}
}
