package com.metamorphosys.interfaces.tranferService;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.metamorphosys.insureconnect.dataobjects.transaction.EntityDocumentDO;
import com.metamorphosys.insureconnect.dataobjects.transaction.EntitySignatureDO;
import com.metamorphosys.insureconnect.jpa.master.ApplicationRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntityDocumentRepository;
import com.metamorphosys.insureconnect.jpa.transaction.EntitySignatureRepository;
import com.metamorphosys.insureconnect.utilities.InsureConnectConstants;
import com.metamorphosys.insureconnect.utilities.SerializationUtility;
import com.metamorphosys.interfaces.tranferdataobjects.FileUploadReplyMessageTO;
import com.metamorphosys.interfaces.tranferdataobjects.FileUploadResponseTO;
import com.metamorphosys.interfaces.tranferdataobjects.UploadFileTO;

@RestController
@RequestMapping("/interface/uploadFile")
public class UploadFileInterface {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UploadFileInterface.class);
	
	String charset = "UTF-8";
    File imageToBeUploaded;
    String url = "";
	HttpStatus status;

	@Autowired
	ApplicationRepository applicationRepo;
	
	@Autowired
	EntityDocumentRepository entityDocRep;
	
	@Autowired
	EntitySignatureRepository entitySignRep;
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> execute(@RequestBody @Valid String json,HttpServletRequest request) throws IOException{
//		if(InsureConnectConstants.DataObjects.ENTITYDOCUMMENTDO.equals(objectName)){
			String parentReferenceID = request.getHeader("referenceId");
			String documentType = request.getHeader("documentType");
			UploadFileTO fileTO = new UploadFileTO();
			EntityDocumentDO documentDO = null;
			EntitySignatureDO signatureDO = null;
			
			HttpHeaders imageHeader = new HttpHeaders();
			if(InsureConnectConstants.DataObjects.ENTITYDOCUMMENTDO.equals(documentType)){
				 documentDO = (EntityDocumentDO) SerializationUtility.getInstance().fromJson(json, EntityDocumentDO.class);
				if(documentDO.getEntityImageMimeType().toUpperCase(Locale.ENGLISH).equals(InsureConnectConstants.IMAGEMIMETYPE.PNG)){
					imageHeader.setContentType(MediaType.IMAGE_PNG);
				}
				if(documentDO.getEntityImageMimeType().toUpperCase(Locale.ENGLISH).equals(InsureConnectConstants.IMAGEMIMETYPE.JPEG)){
					imageHeader.setContentType(MediaType.IMAGE_JPEG);
				}
				fileTO.setReferenceId(parentReferenceID);
				fileTO.setFileId(documentDO.getReferenceID());
				fileTO.setFileType(documentDO.getDocumentSubTypeCd());
				imageToBeUploaded = new File(documentDO.getImageURL());
			}else{
				 signatureDO = (EntitySignatureDO) SerializationUtility.getInstance().fromJson(json, EntitySignatureDO.class);
				if(signatureDO.getEntityImageMimeType().toUpperCase(Locale.ENGLISH).equals(InsureConnectConstants.IMAGEMIMETYPE.PNG)){
					imageHeader.setContentType(MediaType.IMAGE_PNG);
				}
				if(signatureDO.getEntityImageMimeType().toUpperCase(Locale.ENGLISH).equals(InsureConnectConstants.IMAGEMIMETYPE.JPEG)){
					imageHeader.setContentType(MediaType.IMAGE_JPEG);
				}
				fileTO.setReferenceId(parentReferenceID);
				fileTO.setFileId(signatureDO.getReferenceID());
				fileTO.setFileType("SIGNATURE");
				imageToBeUploaded = new File(signatureDO.getImageURL());
				
			}
			
			
			
			url =applicationRepo.findByKey(InsureConnectConstants.Interface.UPLOADPATH).getValue();
			LOGGER.info("Uploading file NUP "+url);
			LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
			
			Resource imgres = new FileSystemResource(imageToBeUploaded);
//			HttpHeaders imageHeader = new HttpHeaders();
			// neeed to read from image. Hard coded as PNG.
			
			HttpEntity<Resource> imgEntity = new HttpEntity<Resource>(imgres,imageHeader);
            map.add("file",imgEntity);
            map.add("referenceId", parentReferenceID); // parent reference if
            map.add("fileId", fileTO.getFileId()); //it's own reference id. For first time it is null/blank
            map.add("fileType", fileTO.getFileType());	   
            map.add("fileName", "");
            HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.MULTIPART_FORM_DATA);
			headers.set("Authorization", request.getHeader("Authorization"));
			headers.set("channel", "mobile");
            HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new    HttpEntity<LinkedMultiValueMap<String, Object>>(
                    map, headers);
			RestTemplate restTemplate = new RestTemplate();
//			String res = restTemplate.postForObject(url, requestEntity, String.class);
			ResponseEntity<String> responseObject = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
			LOGGER.info("Response From NUP "+responseObject);
			status=responseObject.getStatusCode();
			if(!HttpStatus.OK.equals(responseObject.getStatusCode())){
				//documentDO.setUploadStatus(InsureConnectConstants.Status.PENDING);
				return new ResponseEntity(responseObject.getStatusCode());
			}else{
				FileUploadResponseTO fileUploadResponseTO= (FileUploadResponseTO) SerializationUtility.getInstance().fromJson(responseObject.getBody(), FileUploadResponseTO.class);
				HashMap<String, String> hashMap = new HashMap<String, String>();
				String authToken = responseObject.getHeaders().getFirst("Authorization");
				hashMap.put("responseCode", Integer.toString(fileUploadResponseTO.getResponseCode()));
				hashMap.put("statusCode", fileUploadResponseTO.getStatusCode());
				FileUploadReplyMessageTO fileUploadReplyMessageTO = fileUploadResponseTO.getReplyMessageList().get(0);
				
				if(InsureConnectConstants.DataObjects.ENTITYDOCUMMENTDO.equals(documentType)){
					documentDO.setEntityImageName(fileUploadReplyMessageTO.getFileName().toString());
					documentDO.setReferenceID(Integer.valueOf(fileUploadReplyMessageTO.getFileId()).toString());
					documentDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
					entityDocRep.save(documentDO);
				}else{
					signatureDO.setEntityImageName(fileUploadReplyMessageTO.getFileName().toString());
					signatureDO.setReferenceID(Integer.valueOf(fileUploadReplyMessageTO.getFileId()).toString());
					signatureDO.setUploadStatus(InsureConnectConstants.Status.SUCCESS);
					entitySignRep.save(signatureDO);
				}
				
				
				
//				hashMap.put("entityDocument", SerializationUtility.getInstance().toJson(documentDO));
				hashMap.put("imageFileName", fileUploadReplyMessageTO.getFileName().toString());
				hashMap.put("imageReferenceId", Integer.valueOf(fileUploadReplyMessageTO.getFileId()).toString());
				hashMap.put("imageUploadStatus", InsureConnectConstants.Status.SUCCESS);
				String jsonResult = SerializationUtility.getInstance().toJson(hashMap);
				return new ResponseEntity(jsonResult, HttpStatus.OK);
			}
						
			
//			RestTemplate restTemplate = new RestTemplate();
//			HttpHeaders httpHeaders = new HttpHeaders();
//			httpHeaders.set("Authorization", request.getHeader("Authorization"));
////			httpHeaders.set("Content-Type", );
//			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//			UriComponentsBuilder queryParamBuilder = UriComponentsBuilder.fromHttpUrl(url);
//			queryParamBuilder.queryParam("channel", "web");
//			queryParamBuilder.queryParam("language", "in_ID");
////			queryParamBuilder.queryParam("requestId", request.getHeader("requestId"));
//			HttpEntity<Object> entity = new HttpEntity<Object>(fileTO, httpHeaders);
//			ResponseEntity<String> jsonObject = restTemplate.exchange(queryParamBuilder.build().encode().toUri(),
//					HttpMethod.POST, entity, String.class);
//			
//			status=jsonObject.getStatusCode();
//			if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
//						//batch data pending
//			}
			
//		}
//		else{
//			EntitySignatureDO entitySignatureDO= (EntitySignatureDO) SerializationUtility.getInstance().fromJson(json, EntitySignatureDO.class);
//			UploadFileTO fileTO = new UploadFileTO();
//			fileTO.setFileId(entitySignatureDO.getId());
//			fileTO.setFileType("jpg");
//			fileTO.setFile(new File(entitySignatureDO.getImageURL()));
//			
//			String url =applicationRepo.findByKey(InsureConnectConstants.Interface.UPLOADPATH).getValue();
//			
//			RestTemplate restTemplate = new RestTemplate();
//			HttpHeaders httpHeaders = new HttpHeaders();
//			httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//			HttpEntity<Object> entity = new HttpEntity<Object>(fileTO, httpHeaders);
//			ResponseEntity<String> jsonObject = restTemplate.exchange(url,
//					HttpMethod.POST, entity, String.class);
//			
//			status=jsonObject.getStatusCode();
//			if(!HttpStatus.OK.equals(jsonObject.getStatusCode())){
//				//batch data pending
//			}
//		}
	}
}
